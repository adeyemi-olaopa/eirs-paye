﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using System.Text;
using Newtonsoft.Json;
using System.IO;

public partial class PayeInputFile_N : System.Web.UI.Page
{
    private string companyRIN = "";
    SqlConnection con = new SqlConnection(PAYEClass.connection);

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["user_id"] == null)
                {
                    Response.Redirect("Login.aspx");
                }

                Session["EditEMPFlag"] = "1";
                DataTable dt_list = new DataTable();

                txt_tax_year.Items.Add("--Select Year--");
                for (int i = DateTime.Now.Year; i >= 2014; i--)
                {
                    txt_tax_year.Items.Add(i.ToString());
                }

                //  SqlDataAdapter Adp = new SqlDataAdapter("select  BusinessRIN, BusinessName, count(*) as totalcount,Tax_Year,CompanyRIN, Status  from vw_InputFile where TaxMonth='January' group by BusinessRIN ,BusinessName,Tax_Year,CompanyRIN", con);

                //SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year, Status,FiledStatus  from vw_ShowBusiness_PayeInputFile_All order by CompanyRIN desc", con);

                // SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year, Status, case when (FiledStatus!='Filed') then 'UnFiled' else 'Filed' end as FiledStatus  from vw_ShowBusiness_PayeInputFile_All order by CompanyRIN desc", con);

                //  SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year, Status, case when (FiledStatus!='Filed') then 'UnFiled' else 'Filed' end as FiledStatus  from vw_ShowBusiness_PayeInputFile_All_Selected order by CompanyRIN desc", con);
                //  Adp.Fill(dt_list);

                //sourabh kaushik   SqlDataAdapter da = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year, Status, case when (FiledStatus!='Filed') then 'UnFiled' else 'Filed' end as FiledStatus  from vw_ShowBusiness_PayeInputFile_All_Selected order by CompanyRIN desc", con))

                //using (SqlDataAdapter da = new SqlDataAdapter("SELECT DISTINCT b.CompanyName,b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount, ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus FROM ( SELECT  ( SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile Inner join LegacySubmissionsPAYE on PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year ) AS totalcount,vw_Rules_Check.* FROM vw_Rules_Check INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear ) AS b LEFT JOIN (SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile ) AS PayeOuputFile ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year ORDER BY b.CompanyRIN DESC, TaxYear DESC;", con))
                //SqlDataAdapter da = new SqlDataAdapter("SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount, ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus FROM ( SELECT  (SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile Inner join LegacySubmissionsPAYE on PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year ) AS totalcount,vw_Rules_Check.* FROM vw_Rules_Check INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear ) AS b LEFT JOIN(SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile) AS PayeOuputFile ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year  ORDER BY b.CompanyRIN,TaxYear DESC", con);
                //                SqlDataAdapter da = new SqlDataAdapter("SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, (select count(*) from PayeOuputFile as p where" +
                //" p.EmployerRIN = b.CompanyRIN and p.Assessment_Year = b.TaxYear) as totalcount, ISNULL(b.status, 0) AS STATUS," +
                //   "CASE WHEN ISNULL(b.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus from vw_PayeInputFile_N as b   ORDER BY b.CompanyRIN,b.TaxYear DESC", con);

                //da.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;

                //using (da)
                //{
                //    da.SelectCommand.CommandTimeout = 0;
                //    da.Fill(dt_list);
                //}

                
                //var CompanyRIN = "FOR00019";
                //var query = "SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount," + 
                //            "ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus " +
                //            "FROM (SELECT(SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile Inner join LegacySubmissionsPAYE " + 
                //            "ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear " +
                //            "AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN " +
                //            "AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year) AS totalcount, vw_Rules_Check.* FROM vw_Rules_Check " +
                //            "INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN " +
                //            "AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear) AS b " +
                //            "LEFT JOIN(SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile) AS PayeOuputFile " +
                //            "ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year " +
                //            "WHERE b.CompanyRIN = '" + CompanyRIN + "' OR b.BusinessRIN = '" + CompanyRIN + "' ORDER BY b.CompanyRIN,TaxYear DESC";

                //SqlCommand cmd = new SqlCommand(query, con);

                //con.Open();

                //using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                //{
                //    cmd.CommandTimeout = PAYEClass.defaultTimeout;
                //    adapter.Fill(dt_list);
                //}

                //con.Close();
                //con.Dispose();

                Session["dt_l"] = dt_list;
                grd_Company.DataSource = dt_list;
                grd_Company.DataBind();

                int pagesize = grd_Company.Rows.Count;
                int from_pg = 1;
                int to = grd_Company.Rows.Count;
                int totalcount = dt_list.Rows.Count;
                lblpagefrom.Text = from_pg.ToString();
                lblpageto.Text = (from_pg + pagesize - 1).ToString();
                lbltoal.Text = totalcount.ToString();

                if (totalcount < grd_Company.PageSize)
                    div_paging.Style.Add("margin-top", "0px");
                else
                    div_paging.Style.Add("margin-top", "-60px");
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Connection Problem.');</script>", false);
            return;
        }
    }

    private DataTable GetEmployeeCompanies(string companyRIN)
    {
        DataTable responseDt = new DataTable();

        var query = "SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount," +
                        "ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus " +
                        "FROM (SELECT(SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile Inner join LegacySubmissionsPAYE " +
                        "ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear " +
                        "AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN " +
                        "AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year) AS totalcount, vw_Rules_Check.* FROM vw_Rules_Check " +
                        "INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN " +
                        "AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear) AS b " +
                        "LEFT JOIN(SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile) AS PayeOuputFile " +
                        "ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year " +
                        "WHERE b.CompanyRIN = '" + companyRIN + "' ORDER BY b.CompanyRIN,TaxYear DESC";

        //var query = "SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount," +
        //                "ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus " +
        //                "FROM (SELECT(SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile Inner join LegacySubmissionsPAYE " +
        //                "ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear " +
        //                "AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN " +
        //                "AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year) AS totalcount, vw_Rules_Check.* FROM vw_Rules_Check " +
        //                "INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN " +
        //                "AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear) AS b " +
        //                "LEFT JOIN(SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile) AS PayeOuputFile " +
        //                "ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year " +
        //                "WHERE b.CompanyRIN = '" + companyRIN + "' OR b.BusinessRIN = '" + companyRIN + "' ORDER BY b.CompanyRIN,TaxYear DESC";

        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
        {
            cmd.CommandTimeout = PAYEClass.defaultTimeout;
            adapter.Fill(responseDt);
        }

        con.Close();
        con.Dispose();

        return responseDt;
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        grd_Company.PageIndex = e.NewPageIndex;
        grd_Company.DataSource = Session["dt_l"];

        grd_Company.DataBind();

        if (e.NewPageIndex + 1 == 1)
        {
            lblpagefrom.Text = "1";
        }
        else
        {
            lblpagefrom.Text = ((grd_Company.Rows.Count * e.NewPageIndex) + 1).ToString();
        }

        lblpageto.Text = ((e.NewPageIndex + 1) * grd_Company.Rows.Count).ToString();

    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        DataTable dt_list_s = new DataTable();
        dt_list_s = (DataTable)Session["dt_l"];
        DataTable dt_filtered = new DataTable();
        DataView dt_v = dt_list_s.DefaultView;

        if (txt_employer_RIN.Text != "")
        {
            Session["searched_company_RIN"] = txt_employer_RIN.Text;
            var companyRIN = txt_employer_RIN.Text;
            dt_filtered = GetEmployeeCompanies(companyRIN);
            //dt_v.RowFilter = "CompanyRIN like '%" + txt_employer_RIN.Text + "%' or CompanyName like '%" + txt_employer_RIN.Text + "%' or CompanyTIN like '%" + txt_employer_RIN.Text + "%' or Tax_Year like '%" + txt_employer_RIN.Text + "%' or BusinessRIN like '%" + txt_employer_RIN.Text + "%' or FiledStatus like '" + txt_employer_RIN.Text + "%'";

            //if(txt_tax_year.SelectedItem.Text != "--Select Year--")
            //    dt_v.RowFilter = "(CompanyRIN like '%" + txt_employer_RIN.Text + "%' or CompanyName like '%" + txt_employer_RIN.Text + "%' or CompanyTIN like '%" + txt_employer_RIN.Text + "%' or Tax_Year like '%" + txt_employer_RIN.Text + "%' or BusinessRIN like '%" + txt_employer_RIN.Text + "%' or FiledStatus like '" + txt_employer_RIN.Text + "%') and (Tax_Year like '%" + txt_tax_year.SelectedItem.Text + "%')";
        }

        //if (txt_tax_year.SelectedItem.Text != "--Select Year--" && txt_employer_RIN.Text == "")
        //    dt_v.RowFilter = "Tax_Year like '%" + txt_tax_year.SelectedItem.Text + "%'";     

        //grd_Company.DataSource = dt_v;
        grd_Company.DataSource = dt_filtered;
        grd_Company.DataBind();

        int pagesize = grd_Company.Rows.Count;
        int from_pg = 1;
        int to = grd_Company.Rows.Count;
        int totalcount = dt_v.Count;
        lblpagefrom.Text = from_pg.ToString();
        lblpageto.Text = (from_pg + pagesize - 1).ToString();
        lbltoal.Text = totalcount.ToString();

        if (totalcount < grd_Company.PageSize)
            div_paging.Style.Add("margin-top", "0px");
        else
            div_paging.Style.Add("margin-top", "-60px");
    }
    protected void btn_file_selected_Click(object sender, EventArgs e)
    {
       // ScriptManager.RegisterStartupScript(Page, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);

        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Mohan');</script>", false);
        int check = 0;
        foreach (GridViewRow gvrow in grd_Company.Rows)
        {
            
            System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
            if (chk != null & chk.Checked)
            {
                check = 1;
            }
           
           
        }

        if (check == 0)
        {
           
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Select a Company For Filed.');</script>", false);
                return;
            
        }

        string confirmValue = Request.Form["confirm_value"];

         confirmValue = hidden1.Value; 
        if (confirmValue == "Yes")
        {
            foreach (GridViewRow gvrow in grd_Company.Rows)
            {
                System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
                if (chk != null & chk.Checked)
                {
                    string countEMP = gvrow.Cells[6].Text;
                    string CompRIN = "";
                    DataTable dt_count_EMP_10000 = new DataTable();
                    SqlDataAdapter Adp_10000 = new SqlDataAdapter("select count(*) as c, employerrin from PayeOuputFile where Annualgross>=10000 and employerrin='" + gvrow.Cells[0].Text + "' and Assessment_Year='"+ gvrow.Cells[3].Text + "' group by EmployerRIN", con);
                    Adp_10000.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
                    Adp_10000.Fill(dt_count_EMP_10000);

                    if (dt_count_EMP_10000.Rows.Count > 0 && countEMP == dt_count_EMP_10000.Rows[0]["c"].ToString())
                    {
                        SqlCommand delete = new SqlCommand("Update PayeOuputFile set Status=1 where EmployerRIN='" + gvrow.Cells[0].Text + "' and Assessment_Year='" + gvrow.Cells[3].Text + "'", con);
                        con.Open();
                        delete.ExecuteNonQuery();
                        con.Close();
                    }
                    else
                    {
                        CompRIN = CompRIN + "," + gvrow.Cells[0].Text;
                        CompRIN = CompRIN.Trim(',');
                        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Can't Filed these Companies " + CompRIN + " because AnnualGross is less than 10000 of some employees.');</script>", false);
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Can not Filed these Companies " + CompRIN + " because AnnualGross is less than 10000 of some employees.');</script>", false);
                    }
                }
            }

            //DataTable dt_list = new DataTable();
            //SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year, Status,FiledStatus  from vw_ShowBusiness_PayeInputFile order by CompanyRIN desc", con);
            //Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            //Adp.Fill(dt_list);

            var companyRIN = Session["searched_company_RIN"].ToString();
            var dt_filtered = GetEmployeeCompanies(companyRIN);

            Session["dt_l"] = dt_filtered;
            grd_Company.DataSource = dt_filtered;
            grd_Company.DataBind();

        }

    }
    protected void btn_add_new_inputfile_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddPayeInputFile_N.aspx");
    }

    public void checkbox()
    {
    }
    protected void grd_Company_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            CheckBox chkbox = (CheckBox)e.Row.FindControl("chkchkbox");
            LinkButton lnk_SubmitFiling = (LinkButton)e.Row.FindControl("lnksendtoinputfile");
            LinkButton lbtn_drop_emps = (LinkButton)e.Row.FindControl("lnk_drop_employees");
            if (e.Row.Cells[7].Text == "Filed")
            {

                chkbox.Enabled = false;
                lnk_SubmitFiling.Visible = false;

                lbtn_drop_emps.Visible = false;

            }

            else
            {

                chkbox.Enabled = true;
                lnk_SubmitFiling.Visible = true;
                lbtn_drop_emps.Visible = true;
            }

        }
    }

    protected void OnDataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        for (int i = 0; i < grd_Company.Columns.Count; i++)
        {
            TableHeaderCell cell = new TableHeaderCell();
            TextBox txtSearch = new TextBox();
            txtSearch.Attributes["placeholder"] = grd_Company.Columns[i].HeaderText;
            txtSearch.CssClass = "search_textbox";
            cell.Controls.Add(txtSearch);
            row.Controls.Add(cell);
        }
        grd_Company.HeaderRow.Parent.Controls.AddAt(1, row);
    }




    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }

        //  public string Content-type { get; set; }
    }

    public class Receiver
    {
        public string Success { get; set; }
        //public string Message { get; set; }
        //public string Result { get; set; }

    }

    public void getIndData(string CompRIN)
    {
        DataTable dt_list = new DataTable();
        SqlDataAdapter Adp = new SqlDataAdapter("select  distinct TaxPayerRIN, TaxPayerID, BusinessID, BusinessRIN from Businesses_Full_API where TaxPayerRIN not in (select EmployerRIN from PayeOuputFile where EmployerRIN is not null) and TaxPayerRIN='" + CompRIN + "' order by BusinessID desc", con);
        Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
        Adp.Fill(dt_list);

        /***************************************************************/
        string token = PAYEClass.getToken();
        /**************************************************************/
        string URI1 = "";

        URI1 = "https://stage-api.eirsautomation.xyz/TaxPayer/Company/GetAssetTaxPayer?AssetTypeID=3 &AssetID=34268";

        for (int i = 0; i < dt_list.Rows.Count; i++)
        {
            URI1 = "https://stage-api.eirsautomation.xyz/TaxPayer/Company/GetAssetTaxPayer?AssetTypeID=3 &AssetID=" + dt_list.Rows[i]["BusinessID"].ToString() + "";
            URI1 = PAYEClass.URL_API + "TaxPayer/Company/GetAssetTaxPayer?AssetTypeID=3 &AssetID=" + dt_list.Rows[i]["BusinessID"].ToString() + "";
            string InsCompRes = "";
            string headers = "";
            using (var wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

                InsCompRes = wc.DownloadString(URI1);

                var des = (Receiver)JsonConvert.DeserializeObject(InsCompRes, typeof(Receiver));



                DataTable dt_list_ins = new DataTable();

                if (des != null && (des.Success).ToLower().ToString() == "true")
                {
                    if (InsCompRes != "" && InsCompRes.Split('[')[1].ToString() != "]}")
                    {
                        dt_list_ins = (DataTable)JsonConvert.DeserializeObject("[" + InsCompRes.Split('[')[1].Replace("}]", "") + "]", (typeof(DataTable)));
                        // dt_list_ins = (DataTable)JsonConvert.DeserializeObject("[" + InsCompRes.Split('[')[1].Replace("}]", "") + InsCompRes.Split('[')[2].Replace("}]", "") + "]", (typeof(DataTable)));

                        DataRow[] dr_comp = dt_list_ins.Select("TaxPayerTypeID = 2");
                       // Array.ForEach<DataRow>(dt_list_ins.Select("TaxPayerTypeID =2"), row => row.Delete());

                        if (dt_list_ins.Rows.Count > 0)
                        {
                            for (int j = 0; j < dt_list_ins.Rows.Count; j++)
                            {
                                if (dt_list_ins.Rows[j]["TaxPayerTypeID"].ToString() != "2")
                                {
                                    DataTable dt_list_select = new DataTable();
                                    SqlDataAdapter Adp_select = new SqlDataAdapter("select  * from Payeouputfile where EmployeeRIN='" + dt_list_ins.Rows[j]["TaxPayerRINNumber"].ToString() + "'", con);
                                    Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
                                    Adp_select.Fill(dt_list_select);

                                    if (dt_list_select.Rows.Count == 0)
                                    {
                                       // con.Open();
                                        //SqlCommand cmd = new SqlCommand("insert into Payeouputfile(EmployerName,EmployerAddress,EmployerRIN,StartMonth,Nationality,Title,FirstName,MiddleName,Surname,EmployeeRIN,EmployeeTIN,AnnualGross,CRA,ValidatedPension,ValidatedNHF,ValidatedNHIS,TaxFreePay,ChargeableIncome,AnnualTax,MonthlyTax,Assessment_Year,Status) values('" + dr_comp[0][4].ToString().Replace("'", "''") + "','','" + dr_comp[0][5].ToString() + "','January','','','" + dt_list_ins.Rows[j][4].ToString().Replace("'", "''") + "','','','" + dt_list_ins.Rows[j][5].ToString() + "','',0,0,0,0,0,0,0,0,0,'2018',0);", con);
                                       // cmd.ExecuteNonQuery();

                                       // SqlCommand cmd1 = new SqlCommand("insert into LegacySubmissionsPAYE values((select max(LSID)+1 from LegacySubmissionsPAYE),  '" + dt_list_ins.Rows[j][5].ToString() + "', '2018', 0,0,0,0,0,0,0,0,'" + dr_comp[0][14].ToString() + "',1,0,0);", con);
                                       // cmd1.ExecuteNonQuery();

                                      //  SqlCommand cmd2 = new SqlCommand("update Individuals_api set TIN='" + dt_list_ins.Rows[j][5].ToString() + "' where TaxPayerRIN='" + dt_list_ins.Rows[j][5].ToString() + "';", con);
                                       // cmd2.ExecuteNonQuery();


                                        con.Close();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    protected void btn_drop_employees_Click(object sender, EventArgs e)
    {
        GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
        string rin = grd_Company.Rows[clickedRow.RowIndex].Cells[0].Text.ToString();
        string tax_year = grd_Company.Rows[clickedRow.RowIndex].Cells[3].Text.ToString();

        string confirmValue = Request.Form["confirm_value"];

        confirmValue = hidden1.Value;
        if (confirmValue == "Yes")
        {


            string a;
            string main_ret = "";

            SqlConnection cn = new SqlConnection(PAYEClass.connection.ToString());

            try
            {
                SqlCommand cmd = new SqlCommand("Adm_Drop_Emp", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Comp_RIN", rin);
                cmd.Parameters.AddWithValue("@Emp_RIN", "");
                cmd.Parameters.AddWithValue("@TaxYear", tax_year);
                cmd.Parameters.AddWithValue("@flag", "C");
                cmd.CommandTimeout = PAYEClass.defaultTimeout;

                SqlParameter output = new SqlParameter("@SuccessID", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);


                //  cmd.Parameters["@SuccessID"].Direction = ParameterDirection.Output;

                // cmd.Parameters.Add(new SqlParameter("@SuccessID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output);
                cn.Open();
                cmd.ExecuteNonQuery();

                a = output.Value.ToString();
                cn.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "hideImage()", true);
                //Response.Write("<script> alert('Record Inserted Successfully')</script>");

                a = "1";
                if (a == "1")
                {
                    
                    main_ret = main_ret + "{\"Result\":\"Success\"}";
                    DataTable dt_list = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year, Status, case when (FiledStatus!='Filed') then 'UnFiled' else 'Filed' end as FiledStatus  from vw_ShowBusiness_PayeInputFile_All_Selected order by CompanyRIN desc", con);
                    da.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
                    using (da)
                    {

                        da.Fill(dt_list);

                    }





                    Session["dt_l"] = dt_list;
                    grd_Company.DataSource = dt_list;
                    grd_Company.DataBind();

                    int pagesize = grd_Company.Rows.Count;
                    int from_pg = 1;
                    int to = grd_Company.Rows.Count;
                    int totalcount = dt_list.Rows.Count;
                    lblpagefrom.Text = from_pg.ToString();
                    lblpageto.Text = (from_pg + pagesize - 1).ToString();
                    lbltoal.Text = totalcount.ToString();

                    if (totalcount < grd_Company.PageSize)
                        div_paging.Style.Add("margin-top", "0px");
                    else
                        div_paging.Style.Add("margin-top", "-60px");
                }
                else

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Something Went Wrong.');</script>", false);
                    cn.Close();

              
            }

            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "hideImage()", true);
                cn.Close();
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Connection Problem.');</script>", false);
                return;
                
              
            }



        }
    }
}