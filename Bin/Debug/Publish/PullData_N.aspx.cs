﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Threading;
using System.Linq;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Collections;
using Newtonsoft.Json.Linq;

public partial class PullData_N : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(PAYEClass.connection);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }


        if (Session["user_id"] == null)
        {
            Response.Redirect("Login.aspx");

        }
    }


    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ResultProfile
    {
        public int TaxPayerID { get; set; }
        public int TaxPayerTypeID { get; set; }
        public string TaxPayerTypeName { get; set; }
        public string TaxPayerRIN { get; set; }
        public int AssetID { get; set; }
        public int AssetTypeID { get; set; }
        public string AssetTypeName { get; set; }
        public string AssetRIN { get; set; }
        public int ProfileID { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileDescription { get; set; }
        public int TaxPayerRoleID { get; set; }
        public string TaxPayerRoleName { get; set; }
    }

    public class ProfileResponse
    {
        public bool Success { get; set; }
        public object Message { get; set; }
        public List<ResultProfile> Result { get; set; }
    }









    public class ResultCorporate
    {
        public int TaxPayerID { get; set; }
        public int TaxPayerTypeID { get; set; }
        public string TaxPayerTypeName { get; set; }
        public string TaxPayerName { get; set; }
        public string TaxPayerRIN { get; set; }
        public string MobileNumber { get; set; }
        public string ContactAddress { get; set; }
        public string EmailAddress { get; set; }
        public string TIN { get; set; }
        public string TaxOffice { get; set; }
    }

    public class CorporateResponse
    {
        public bool Success { get; set; }
        public object Message { get; set; }
        public List<ResultCorporate> Result { get; set; }
    }





    public class WebClientWithTimeout : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest wr = base.GetWebRequest(address);
            wr.Timeout = 5 * 600000; // timeout in milliseconds (ms)
            return wr;
        }
    }


    public class ResultIndividual
    {
        public int TaxPayerID { get; set; }
        public int TaxPayerTypeID { get; set; }
        public string TaxPayerTypeName { get; set; }
        public string TaxPayerName { get; set; }
        public string TaxPayerRIN { get; set; }
        public string MobileNumber { get; set; }
        public string ContactAddress { get; set; }
        public string EmailAddress { get; set; }
        public string TIN { get; set; }
        public string TaxOffice { get; set; }
    }

    public class IndividualResponse
    {
        public bool Success { get; set; }
        public object Message { get; set; }
        public List<ResultIndividual> Result { get; set; }
    }




















    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }

        //  public string Content-type { get; set; }
    }
    public class Receiver
    {
        public string Success { get; set; }
        //public string Message { get; set; }
        //public string Result { get; set; }

    }
    public class AssestResult
    {
        public int TaxPayerID { get; set; }
        public int TaxPayerTypeID { get; set; }
        public string TaxPayerTypeName { get; set; }
        public string TaxPayerName { get; set; }
        public string TaxPayerRIN { get; set; }
        public int BusinessID { get; set; }
        public int AssetTypeID { get; set; }
        public string AssetTypeName { get; set; }
        public int BusinessTypeID { get; set; }
        public string BusinessTypeName { get; set; }
        public string BusinessRIN { get; set; }
        public string BusinessName { get; set; }
        public int LGAID { get; set; }
        public string LGAName { get; set; }
        public int BusinessCategoryID { get; set; }
        public string BusinessCategoryName { get; set; }
        public int BusinessSectorID { get; set; }
        public string BusinessSectorName { get; set; }
        public int BusinessSubSectorID { get; set; }
        public string BusinessSubSectorName { get; set; }
        public int BusinessStructureID { get; set; }
        public string BusinessStructureName { get; set; }
        public int BusinessOperationID { get; set; }
        public string BusinessOperationName { get; set; }
        public int SizeID { get; set; }
        public string SizeName { get; set; }
        public string ContactName { get; set; }
        public string BusinessNumber { get; set; }
        public string BusinessAddress { get; set; }
    }

    public class Result
    {

        public string TaxPayerID { get; set; }
        public string TaxPayerTypeID { get; set; }
        public string TaxPayerTypeName { get; set; }
        public string TaxPayerRIN { get; set; }
        public string AssetID { get; set; }
        public string AssetTypeID { get; set; }
        public string AssetTypeName { get; set; }
        public string AssetRIN { get; set; }
        public string ProfileID { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileDescription { get; set; }
        public string AssessmentRuleID { get; set; }
        public string AssessmentRuleCode { get; set; }
        public string AssessmentRuleName { get; set; }
        public string AssessmentItemID { get; set; }
        public string AssessmentItemReferenceNo { get; set; }
        public string AssessmentGroupID { get; set; }
        public string AssessmentGroupName { get; set; }
        public string AssessmentSubGroupID { get; set; }
        public string AssessmentSubGroupName { get; set; }
        public string RevenueStreamID { get; set; }
        public string RevenueStreamName { get; set; }
        public string RevenueSubStreamID { get; set; }
        public string RevenueSubStreamName { get; set; }
        public string AssessmentItemCategoryID { get; set; }
        public string AssessmentItemCategoryName { get; set; }
        public string AssessmentItemSubCategoryID { get; set; }
        public string AssessmentItemSubCategoryName { get; set; }
        public string AgencyID { get; set; }
        public string AgencyName { get; set; }
        public string AssessmentItemName { get; set; }
        public string ComputationID { get; set; }
        public string ComputationName { get; set; }
        public string TaxBaseAmount { get; set; }
        public string Percentage { get; set; }
        public string TaxAmount { get; set; }

    }
    public class MainResponse
    {
        public string Success { get; set; }
        public string Message { get; set; }
        public List<Result> Result { get; set; }

    }
    public class MainResponseAsset
    {
        public string Success { get; set; }
        public string Message { get; set; }
        public List<AssestResult> Result { get; set; }

    }

    public class MainResponseRules
    {
        public string Success { get; set; }
        public string Message { get; set; }
        public List<RulesResult> Result { get; set; }

    }

    public class RulesResult
    {
        public string TaxPayerID { get; set; }
        public string TaxPayerTypeID { get; set; }
        public string TaxPayerTypeName { get; set; }
        public string TaxPayerRIN { get; set; }
        public string AssetID { get; set; }
        public string AssetTypeID { get; set; }
        public string AssetTypeName { get; set; }
        public string AssetRIN { get; set; }
        public string ProfileID { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileDescription { get; set; }
        public string AssessmentRuleID { get; set; }
        public string AssessmentRuleCode { get; set; }
        public string AssessmentRuleName { get; set; }
        public string RuleRunID { get; set; }
        public string RuleRunName { get; set; }
        public string PaymentFrequencyID { get; set; }
        public string PaymentFrequencyName { get; set; }
        public string AssessmentAmount { get; set; }
        public string TaxYear { get; set; }
        public string PaymentOptionID { get; set; }
        public string PaymentOptionName { get; set; }
        public string TaxMonth { get; set; }
    }

    public string upload_json_Corporates(string json)
    {
        /***************************************************************/
        string token = PAYEClass.getToken();
        /**************************************************************/

        string URI1 = "";

        if (json == "Corporates")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_TaxPayer";
        }
        if (json == "CorporatesB")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_TaxPayer";
        }
        if (json == "CorporatesC")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_H_Z_TaxPayer";
        }
        if (json == "Business")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_Asset";
        }
        if (json == "Rules")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_AssessmentRule?pageNumber=1&pageSize=140000";
        }

        if (json == "Items")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_AssessmentItem?pageNumber=1&pageSize=1000";

        }
        if (json == "Profile")
        {
            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_Profile";
        }
        if (json == "Individuals")
        {

            URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Contribution_Formal_Business_Employee_BS_A_F_TaxPayer";

        }
        string myParameters1 = "";

        string InsCompRes = "";
        string headers = "";
        MainResponseRules mainResponse = new MainResponseRules { };
        mainResponse.Result = new List<RulesResult>();
        using (var wc = new WebClient())
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            InsCompRes = wc.DownloadString(URI1);

            if (json == "Rules")
            {
                MainResponseRules response = JsonConvert.DeserializeObject<MainResponseRules>(InsCompRes);
                List<RulesResult> listAssetItems = new List<RulesResult>();
                listAssetItems = response.Result;
                mainResponse.Message = response.Message;
                mainResponse.Success = response.Success;
                mainResponse.Result.AddRange(listAssetItems);
                headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();
            }
        }

        //|| json == "Individuals"
        if (json == "Rules")
        {

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));
            int total_pages = Convert.ToInt32(dttt.Rows[0]["totalPages"].ToString());
            string nextpage = dttt.Rows[0]["nextPage"].ToString();

            // int i = 1; SSS
            for (int i = 2; i <= total_pages; i++)
            {


                URI1 = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_AssessmentRule?pageNumber=" + i + "&pageSize=140000";


                using (var wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

                    //  InsCompRes = InsCompRes + wc.DownloadString(URI1);

                    InsCompRes = wc.DownloadString(URI1);

                    MainResponseRules response = JsonConvert.DeserializeObject<MainResponseRules>(InsCompRes);
                    List<RulesResult> listAssetItems = new List<RulesResult>();
                    listAssetItems = response.Result;
                    mainResponse.Message = response.Message;
                    mainResponse.Success = response.Success;
                    mainResponse.Result.AddRange(listAssetItems);
                    //headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

                    //dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));
                    //nextpage = dttt.Rows[0]["nextPage"].ToString();
                }


            }


            json = JsonConvert.SerializeObject(mainResponse);
        }
        else
        {
            json = InsCompRes;
        }






        WebClient request = new WebClient();




        //string url = PAYEClass.uploadurltxtfile + "AssessmentRules.txt";
        //string version = "";
        //string fileString = "";
        //request.Credentials = new NetworkCredential(PAYEClass.ftpusername, PAYEClass.ftppassword);

        //try
        //{
        //    //  byte[] data = Encoding.ASCII.GetBytes(json);
        //    //  request.UploadData(new Uri(url), data);

        //    StringBuilder sw = new StringBuilder();
        //  //  sw.Append(System.IO.File.ReadAllText(Server.MapPath("~") + "/App_Code/AssessmentRules.txt"));

        //    byte[] data = request.DownloadData(new Uri(url));
        //    InsCompRes = Encoding.UTF8.GetString(data);

        //    json = InsCompRes;
        //}

        //catch (WebException e)
        //{
        //    div_loading.Attributes.Add("display", "none");
        //}
        return json;
    }

    public void insert_corporates(DataTable table)
    {
        try
        {

            SqlConnection con1 = new SqlConnection(PAYEClass.connection);

            //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE CompanyList_API", con);
            //con.Open();
            //truncate.ExecuteNonQuery();
            //con.Close();

            SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='CompanyList_API'", con);
            con.Open();
            update_tables_API_Updated.ExecuteNonQuery();
            con.Close();



            // Array.ForEach<DataRow>(table.Select("TaxPayerRIN IS NULL"), row => row.Delete());
            Array.ForEach<DataRow>(table.Select("TaxPayerTypeID IS NULL"), row => row.Delete());
            Array.ForEach<DataRow>(table.Select("TaxPayerID IS NULL"), row => row.Delete());


            using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
            {
                //con.Open();
                // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                foreach (DataColumn col in table.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                bulkCopy.BulkCopyTimeout = 600;
                bulkCopy.DestinationTableName = "CompanyList_API";
                bulkCopy.WriteToServer(table);
            }

            showmsg(1, "TaxPayer-Corporats Synced Successfully");
        }
        catch (Exception ex)
        {
            showmsg(2, "Error Occured. Contact to Administrator.");
        }
    }




    public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
    {
        return dt.DefaultView.ToTable(true, Columns);
    }




    public void showmsg(int id, string msg)
    {
        if (id == 1)
        {
            divmsg.Style.Add("display", "block");
            divmsg.InnerHtml = "<i class='menu-icon fa fa-check-circle' style='font-size:20px !important;'></i>&nbsp;" + msg + "";
            divmsg.Attributes.Add("class", "alert alert-success");
        }
        else if (id == 2)
        {
            divmsg.Style.Add("display", "block");
            divmsg.InnerHtml = "<i class='menu-icon fa fa-warning (alias)' style='font-size:20px !important;'></i>&nbsp;" + msg + "";
            divmsg.Attributes.Add("class", "alert alert-warning");
        }
        else
        {
            divmsg.Style.Add("display", "none");
        }
    }



    [System.Web.Services.WebMethod]
    public static void truncate_data()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Assessment_Item_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Assessment_Item_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }

    [System.Web.Services.WebMethod]
    public static void truncate_dataB()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Assessment_Item_API", con); //I need to check this query
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Assessment_Item_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_dataC()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Assessment_Item_API", con); //I need to check this query
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Assessment_Item_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }

    [System.Web.Services.WebMethod]
    public static void truncate_assets_data()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Businesses_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Businesses_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_assets_dataB()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Businesses_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Businesses_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_assets_dataC()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Businesses_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Businesses_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }

    [System.Web.Services.WebMethod]
    public static void truncate_Individual_data()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        ////truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Individuals_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Individuals_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_Individual_dataB()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        ////truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Individuals_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Individuals_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_Individual_dataC()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        ////truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Individuals_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Individuals_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }


    [System.Web.Services.WebMethod]
    public static void truncate_profile_data()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        ////truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Profiles_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Profiles_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_profile_dataB()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        ////truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Profiles_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Profiles_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }
    [System.Web.Services.WebMethod]
    public static void truncate_profile_dataC()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        ////truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Profiles_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Profiles_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }

    [System.Web.Services.WebMethod]
    public static void truncate_corporate_data()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        //truncating items table to insert new data
        //SqlCommand truncate = new SqlCommand("TRUNCATE TABLE CompanyList_API", con);
        //con.Open();
        //truncate.ExecuteNonQuery();
        //con.Close();

        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='CompanyList_API'", con);
        con.Open();
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }


    [System.Web.Services.WebMethod]
    public static void truncate_Rules_data()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        con.Open();
        DateTime curretnDate = DateTime.Today; ; // random date
        string year = curretnDate.ToString("yyyy");
        //truncating items table to insert new data
        if (PAYEClass.URL_API.Contains("stage"))
        {
            SqlCommand truncate = new SqlCommand("DELETE FROM AssessmentRules WHERE TaxYear =  '2019'", con);

            truncate.ExecuteNonQuery();
        }
        else
        {
            SqlCommand truncate = new SqlCommand("DELETE FROM AssessmentRules WHERE TaxYear =  " + year, con);
            truncate.ExecuteNonQuery();
        }



        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='AssessmentRules'", con);
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }

    [System.Web.Services.WebMethod]
    public static void truncate_Rules_dataB()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        con.Open();
        DateTime curretnDate = DateTime.Today; ; // random date
        string year = curretnDate.ToString("yyyy");
        //truncating items table to insert new data
        if (PAYEClass.URL_API.Contains("stage"))
        {
            SqlCommand truncate = new SqlCommand("DELETE FROM AssessmentRules WHERE TaxYear =  '2019'", con);

            truncate.ExecuteNonQuery();
        }
        else
        {
            SqlCommand truncate = new SqlCommand("DELETE FROM AssessmentRules WHERE TaxYear =  " + year, con);
            truncate.ExecuteNonQuery();
        }



        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='AssessmentRules'", con);
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }

    [System.Web.Services.WebMethod]
    public static void truncate_Rules_dataC()
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        con.Open();
        DateTime curretnDate = DateTime.Today; ; // random date
        string year = curretnDate.ToString("yyyy");
        //truncating items table to insert new data
        if (PAYEClass.URL_API.Contains("stage"))
        {
            SqlCommand truncate = new SqlCommand("DELETE FROM AssessmentRules WHERE TaxYear =  '2019'", con);

            truncate.ExecuteNonQuery();
        }
        else
        {
            SqlCommand truncate = new SqlCommand("DELETE FROM AssessmentRules WHERE TaxYear =  " + year, con);
            truncate.ExecuteNonQuery();
        }



        //updating items table last updated date
        SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='AssessmentRules'", con);
        update_tables_API_Updated.ExecuteNonQuery();
        con.Close();

    }


    public static string initialize_syncing_items(int pageNumber, int pageSize, int det)
    {

        //hitting login API to get token
        string token = PAYEClass.getToken();

        //starting recursive api hits
        if (det == 1)
            return start_Api_hits_items(pageNumber, token, pageSize, 1);

        else if (det == 2)
            return start_Api_hits_items(pageNumber, token, pageSize, 2);

        return start_Api_hits_items(pageNumber, token, pageSize, 3);




    }
    public static string initialize_syncing_Assets(int pageNumber, int pageSize, int det)
    {

        //hitting login API to get token
        string token = PAYEClass.getToken();
        if (det == 1)
        {
            return start_Api_hits_assets(pageNumber, token, pageSize, 1);
        }
        else if (det == 2)
        {
            return start_Api_hits_assets(pageNumber, token, pageSize, 2);

        }
        //starting recursive api hits
        return start_Api_hits_assets(pageNumber, token, pageSize, 3);
    }
    public static string initialize_syncing_profile(int pageNumber, int pageSize, int det)
    {

        //hitting login API to get token
        string token = PAYEClass.getToken();

        //starting recursive api hits
        if (det == 1)
            return start_Api_hits_profile(pageNumber, token, pageSize, 1);
        else if (det == 2)
            return start_Api_hits_profile(pageNumber, token, pageSize, 2);
        else
            return start_Api_hits_profile(pageNumber, token, pageSize, 3);

    }

    public static string initialize_syncing_Corporate(int pageNumber, int pageSize, int det)
    {

        //hitting login API to get token
        string token = PAYEClass.getToken();

        if (det == 1)
        {
            return start_Api_hits_corporate(pageNumber, token, pageSize, 1);
        }
        else if (det == 2)
        {
            return start_Api_hits_corporate(pageNumber, token, pageSize, 2);

        }

        return start_Api_hits_corporate(pageNumber, token, pageSize, 3);
    }

    public static string initialize_syncing_Individual(int pageNumber, int pageSize, int det)
    {

        //hitting login API to get token
        string token = PAYEClass.getToken();

        //starting recursive api hits
        if (det == 1)
        {
            return start_Api_hits_individual(pageNumber, token, pageSize, 1);
        }
        else if (det == 2)
        {
            return start_Api_hits_individual(pageNumber, token, pageSize, 2);

        }
        return start_Api_hits_individual(pageNumber, token, pageSize, 3);


    }

    public static string initialize_rules_syncing_items(int pageNumber, int pageSize, int det)
    {

        //hitting login API to get token
        string token = PAYEClass.getToken();

        //starting recursive api hits
        if (det == 1)
            return start_Api_hits_rules(pageNumber, token, pageSize, 1);
        else if (det == 2)
            return start_Api_hits_rules(pageNumber, token, pageSize, 2);
        else
            return start_Api_hits_rules(pageNumber, token, pageSize, 3);
    }




    [System.Web.Services.WebMethod]
    public static string checkProgress(int pageNumber, int pageSize)
    {
        return initialize_syncing_items(pageNumber, pageSize, 1);
    }

    [System.Web.Services.WebMethod]
    public static string checkProgressB(int pageNumber, int pageSize)
    {
        return initialize_syncing_items(pageNumber, pageSize, 2);
    }
    [System.Web.Services.WebMethod]
    public static string checkProgressC(int pageNumber, int pageSize)
    {
        return initialize_syncing_items(pageNumber, pageSize, 3);
    }


    [System.Web.Services.WebMethod]
    public static string AssetcheckProgress(int pageNumber, int pageSize)
    {
        return initialize_syncing_Assets(pageNumber, pageSize, 1); ;
    }
    [System.Web.Services.WebMethod]
    public static string AssetcheckProgressB(int pageNumber, int pageSize)
    {
        return initialize_syncing_Assets(pageNumber, pageSize, 2); ;
    }
    [System.Web.Services.WebMethod]
    public static string AssetcheckProgressC(int pageNumber, int pageSize)
    {
        return initialize_syncing_Assets(pageNumber, pageSize, 3); ;
    }


    [System.Web.Services.WebMethod]
    public static string CorporatecheckProgress(int pageNumber, int pageSize)
    {
        return initialize_syncing_Corporate(pageNumber, pageSize, 1);
    }
    [System.Web.Services.WebMethod]
    public static string CorporatecheckProgressB(int pageNumber, int pageSize)
    {
        return initialize_syncing_Corporate(pageNumber, pageSize, 2);
    }
    [System.Web.Services.WebMethod]
    public static string CorporatecheckProgressC(int pageNumber, int pageSize)
    {
        return initialize_syncing_Corporate(pageNumber, pageSize, 3);
    }

    [System.Web.Services.WebMethod]
    public static string IndividualcheckProgress(int pageNumber, int pageSize)
    {
        return initialize_syncing_Individual(pageNumber, pageSize, 1); ;
    }

    [System.Web.Services.WebMethod]
    public static string IndividualcheckProgressB(int pageNumber, int pageSize)
    {
        return initialize_syncing_Individual(pageNumber, pageSize, 2); ;
    }

    [System.Web.Services.WebMethod]
    public static string IndividualcheckProgressC(int pageNumber, int pageSize)
    {
        return initialize_syncing_Individual(pageNumber, pageSize, 3); ;
    }

    [System.Web.Services.WebMethod]
    public static string ProfilecheckProgress(int pageNumber, int pageSize)
    {
        return initialize_syncing_profile(pageNumber, pageSize, 1); ;
    }

    [System.Web.Services.WebMethod]
    public static string ProfilecheckProgressB(int pageNumber, int pageSize)
    {
        return initialize_syncing_profile(pageNumber, pageSize, 2); ;
    }

    [System.Web.Services.WebMethod]
    public static string ProfilecheckProgressC(int pageNumber, int pageSize)
    {
        return initialize_syncing_profile(pageNumber, pageSize, 3); ;
    }

    [System.Web.Services.WebMethod]
    public static string rulesCheckProgress(int pageNumber, int pageSize)
    {
        return initialize_rules_syncing_items(pageNumber, pageSize, 1); ;
    }

    [System.Web.Services.WebMethod]
    public static string rulesCheckProgressB(int pageNumber, int pageSize)
    {
        return initialize_rules_syncing_items(pageNumber, pageSize, 2); ;
    }

    [System.Web.Services.WebMethod]
    public static string rulesCheckProgressC(int pageNumber, int pageSize)
    {
        return initialize_rules_syncing_items(pageNumber, pageSize, 3); ;
    }



    public static DataTable RemoveDuplicateRows(DataTable dTable, string colName)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();

        //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
        //And add duplicate item value in arraylist.
        foreach (DataRow drow in dTable.Rows)
        {
            if (hTable.Contains(drow[colName]))
                duplicateList.Add(drow);
            else
                hTable.Add(drow[colName], string.Empty);
        }

        //Removing a list of duplicate items from datatable.
        foreach (DataRow dRow in duplicateList)
            dTable.Rows.Remove(dRow);

        //Datatable which contains unique records will be return as output.
        return dTable;
    }

    public static string start_Api_hits_rules(int page_number, String token, int pageSize, int det)
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);

        string URL = "";
        if (det == 1)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_AssessmentRule?pageNumber=" + page_number + "&pageSize=" + pageSize;
        if (det == 2)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_AssessmentRule?pageNumber=" + page_number + "&pageSize=" + pageSize;
        if (det == 3)
            URL = PAYEClass.URL_API + "SupplierData//PAYE_Collection_Multiple_Employees_BS_H_Z_AssessmentRule?pageNumber=" + page_number + "&pageSize=" + pageSize;

        MainResponseRules mainResponse = new MainResponseRules { };
        mainResponse.Result = new List<RulesResult>();
        using (var wc = new WebClientWithTimeout())
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            // setting value in model

            string InsCompRes = wc.DownloadString(URL);

            MainResponseRules response = JsonConvert.DeserializeObject<MainResponseRules>(InsCompRes);
            List<RulesResult> listAssetItems = new List<RulesResult>();
            listAssetItems = response.Result;
            mainResponse.Message = response.Message;
            mainResponse.Success = response.Success;
            mainResponse.Result.AddRange(listAssetItems);

            //inserting data in DB
            string deserializeResponse = JsonConvert.SerializeObject(mainResponse);
            DataTable dt_list = (DataTable)JsonConvert.DeserializeObject("[" + deserializeResponse.Split('[')[1].Replace("}]", "").Replace(deserializeResponse.Split('[')[0].Replace("}]", ""), "") + "]", (typeof(DataTable)));
            System.Data.DataColumn newColumn = new System.Data.DataColumn("Active", typeof(System.String));
            newColumn.DefaultValue = "1";
            dt_list.Columns.Add(newColumn);

            string[] distinct = { "TaxPayerID", "TaxPayerTypeID", "TaxPayerTypeName", "TaxPayerRIN", "AssetID", "AssetTypeID", "AssetTypeName", "AssetRIN", "ProfileID", "ProfileReferenceNo", "ProfileDescription", "AssessmentRuleID", "AssessmentRuleCode", "AssessmentRuleName", "RuleRunID", "RuleRunName", "PaymentFrequencyID", "PaymentFrequencyName", "AssessmentAmount", "TaxYear", "PaymentOptionID", "PaymentOptionName", "TaxMonth" };
            DataTable dtDistinct_insert = GetDistinctRecords(dt_list, distinct);
            bluck_inset_rules(dtDistinct_insert, det);

            //checking if next page is available

            string headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));

            string nextpage = dttt.Rows[0]["nextPage"].ToString();
            if (nextpage != "Yes")
                RuleCount = 0;
            return headers;
        }
    }

    //check if it exist in db update else insert where taxpayerid, is same if assest assetid, profile profileid
    private static int RuleCount = 0;
    public static void bluck_inset_rules(DataTable table, int det)
    {
        SqlConnection con1 = new SqlConnection(PAYEClass.connection);

        using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
        {
            if (RuleCount < 1)
            {
                con1.Open();
                SqlCommand command = new SqlCommand("Delete from Assessment_Rules_API where ApiId =" + det, con1);
                command.CommandTimeout = 60 * 600;
                command.ExecuteNonQuery();
                con1.Close();
            }


            con1.Open();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("insert into Assessment_Rules_API values("
                    + Convert.ToInt32(table.Rows[i]["TaxPayerID"])
                    + "," + Convert.ToInt32(table.Rows[i]["TaxPayerTypeID"])
                    + ",'" + table.Rows[i]["TaxPayerTypeName"].ToString()
                    + "','" + table.Rows[i]["TaxPayerRIN"].ToString()
                    + "'," + Convert.ToInt32(table.Rows[i]["AssetID"])
                    + "," + Convert.ToInt32(table.Rows[i]["AssetTypeID"])
                    + ",'" + table.Rows[i]["AssetTypeName"].ToString()
                    + "','" + table.Rows[i]["AssetRIN"].ToString()
                    + "'," + Convert.ToInt32(table.Rows[i]["ProfileID"])
                    + ",'" + table.Rows[i]["ProfileReferenceNo"].ToString()
                    + "','" + table.Rows[i]["ProfileDescription"].ToString()
                    + "','" + table.Rows[i]["AssessmentRuleID"].ToString()
                    + "','" + table.Rows[i]["AssessmentRuleCode"].ToString()
                    + "','" + table.Rows[i]["AssessmentRuleName"].ToString()
                    + "'," + Convert.ToInt32(table.Rows[i]["RuleRunID"])
                    + ",'" + table.Rows[i]["RuleRunName"].ToString()
                    + "'," + Convert.ToInt32(table.Rows[i]["PaymentFrequencyID"])
                    + ",'" + table.Rows[i]["PaymentFrequencyName"].ToString()
                    + "'," + Convert.ToInt32(table.Rows[i]["AssessmentAmount"])
                    + ",'" + table.Rows[i]["TaxYear"].ToString()
                    + "'," + Convert.ToInt32(table.Rows[i]["PaymentOptionID"])
                    + ",'" + table.Rows[i]["PaymentOptionName"].ToString()
                    + "','" + table.Rows[i]["TaxMonth"].ToString()
                    + "'," + det
                    + ");", con1);
                cmd.CommandTimeout = 2 * 600;
                cmd.ExecuteNonQuery();
            }
            con1.Close();
        }
    }

    public static string start_Api_hits_assets(int page_number, String token, int pageSize, int det)
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        string URL = "";
        if (det == 1)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_Asset?pageNumber=" + page_number + "&pageSize=" + pageSize;
        else if (det == 2)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_Asset?pageNumber=" + page_number + "&pageSize=" + pageSize;
        else
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_H_Z_Asset?pageNumber=" + page_number + "&pageSize=" + pageSize;


        using (var wc = new WebClientWithTimeout())
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            // setting value in model
            String InsCompRes = wc.DownloadString(URL);

            MainResponseAsset response = JsonConvert.DeserializeObject<MainResponseAsset>(InsCompRes);
            //inserting data in DB
            string deserializeResponse = JsonConvert.SerializeObject(response);
            DataTable dt_list = (DataTable)PAYEClass.ToDataTable(response.Result);

            string[] distinct = {
                "BusinessID",
                "BusinessRIN",
                "AssetTypeID",
                "AssetTypeName",
                "BusinessTypeID",
                "BusinessTypeName",
                "BusinessName",
                "LGAID",
                "LGAName",
                "BusinessCategoryID",
                "BusinessCategoryName",
                "BusinessSectorID",
                "BusinessSectorName",
                "BusinessSubSectorID",
                "BusinessSubSectorName",
                "BusinessStructureID",
                "BusinessStructureName",
                "BusinessOperationID",
                "BusinessOperationName",
                "SizeID",
                "SizeName",
                "ContactName",
                "BusinessNumber",
                "BusinessAddress"
            };
            DataTable dtDistinct_insert = GetDistinctRecords(dt_list, distinct);
            bluck_inset_Assets(dtDistinct_insert, det);


            //checking if next page is available

            string headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));

            string nextpage = dttt.Rows[0]["nextPage"].ToString();
            if (nextpage != "Yes")
                AssetCount = 0;

            return headers;


        }
    }
    public static string start_Api_hits_profile(int page_number, String token, int pageSize, int det)
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        string URL = "";
        if (det == 1)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_Profile?pageNumber=" + page_number + "&pageSize=" + pageSize;
        else if (det == 2)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_Profile?pageNumber=" + page_number + "&pageSize=" + pageSize;
        else
            URL = URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_H_Z_Profile?pageNumber=" + page_number + "&pageSize=" + pageSize;


        using (var wc = new WebClientWithTimeout())
        {


            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            // setting value in model
            String InsCompRes = wc.DownloadString(URL);
            ProfileResponse mainResponse = JsonConvert.DeserializeObject<ProfileResponse>(InsCompRes);
            DataTable dt_list = (DataTable)PAYEClass.ToDataTable(mainResponse.Result);

            string[] TobeDistinct = { "ProfileID", "ProfileReferenceNo", "ProfileDescription" };
            string[] distinct = { "ProfileID", "ProfileReferenceNo", "ProfileDescription" };
            DataTable dtDistinct_insert = GetDistinctRecords(dt_list, distinct);
            bluck_inset_Profile(dtDistinct_insert, det);
            //checking if next page is available
            string headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));

            string nextpage = dttt.Rows[0]["nextPage"].ToString();
            if (nextpage != "Yes")
                ProfileCount = 0;


            return headers;


        }
    }

    public static string start_Api_hits_corporate(int page_number, String token, int pageSize, int det)
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        string URL = "";
        if (det == 1)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_TaxPayer?pageNumber=" + page_number + "&pageSize=" + pageSize;
        else if (det == 2)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_TaxPayer?pageNumber=" + page_number + " &pageSize=" + pageSize;
        else
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_H_Z_TaxPayer?pageNumber=" + page_number + "&pageSize=" + pageSize;


        using (var wc = new WebClientWithTimeout())
        {


            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            // setting value in model
            String InsCompRes = wc.DownloadString(URL);
            CorporateResponse mainResponse = JsonConvert.DeserializeObject<CorporateResponse>(InsCompRes);
            DataTable dt_list = (DataTable)PAYEClass.ToDataTable(mainResponse.Result);
            string[] distinct = {
                "TaxPayerID",
                "TaxPayerTypeID",
                "TaxPayerTypeName",
                "TaxPayerName",
                "TaxPayerRIN",
                "MobileNumber",
                "ContactAddress",
                "EmailAddress",
                "TIN",
                "TaxOffice"
                };
            DataTable dtDistinct_insert = GetDistinctRecords(dt_list, distinct);
            bluck_inset_Corporate(dtDistinct_insert, det);
            //checking if next page is available

            string headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));

            string nextpage = dttt.Rows[0]["nextPage"].ToString();
            if (nextpage != "Yes")
                CorporateCount = 0;


            return headers;


        }
    }

    public static string start_Api_hits_individual(int page_number, String token, int pageSize, int det)
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);

        string URL = "";
        if (det == 1)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Contribution_Formal_Business_Employee_BS_A_F_TaxPayer?pageNumber=" + page_number + "&pageSize=" + pageSize;
        else if (det == 2)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_TaxPayer?pageNumber=" + page_number + " &pageSize=" + pageSize;
        else
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Contribution_Formal_Business_Employee_BS_H_Z_TaxPayer?pageNumber=" + page_number + "&pageSize=" + pageSize;



        using (var wc = new WebClientWithTimeout())
        {


            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            // setting value in model
            String InsCompRes = wc.DownloadString(URL);
            IndividualResponse mainResponse = JsonConvert.DeserializeObject<IndividualResponse>(InsCompRes);
            DataTable dt_list = (DataTable)PAYEClass.ToDataTable(mainResponse.Result);
            string[] distinct = {
                "TaxPayerID",
                "TaxPayerTypeID",
                "TaxPayerTypeName",
                "TaxPayerName",
                "TaxPayerRIN",
                "MobileNumber",
                "ContactAddress",
                "EmailAddress",
                "TIN",
                "TaxOffice"
                 };
            DataTable dtDistinct_insert = GetDistinctRecords(dt_list, distinct);
            bluck_inset_individual(dtDistinct_insert, det);
            //checking if next page is available

            string headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));

            string nextpage = dttt.Rows[0]["nextPage"].ToString();
            if (nextpage != "Yes")
                IndividualCount = 0;

            return headers;


        }
    }

    public static string start_Api_hits_items(int page_number, String token, int pageSize, int det)
    {
        SqlConnection con = new SqlConnection(PAYEClass.connection);
        string URL = "";
        if (det == 1)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_A_F_AssessmentItem?pageNumber=" + page_number + "&pageSize=" + pageSize;

        else if (det == 2)
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_G_AssessmentItem?pageNumber=" + page_number + "&pageSize=" + pageSize;

        else
            URL = PAYEClass.URL_API + "SupplierData/PAYE_Collection_Multiple_Employees_BS_H_Z_AssessmentItem?pageNumber=" + page_number + "&pageSize=" + pageSize;


        MainResponse mainResponse = new MainResponse { };
        mainResponse.Result = new List<Result>();
        using (var wc = new WebClientWithTimeout())
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;

            // setting value in model
            string InsCompRes = wc.DownloadString(URL);

            MainResponse response = JsonConvert.DeserializeObject<MainResponse>(InsCompRes);
            List<Result> listAssetItems = new List<Result>();
            listAssetItems = response.Result;
            mainResponse.Message = response.Message;
            mainResponse.Success = response.Success;
            mainResponse.Result.AddRange(listAssetItems);

            //inserting data in DB
            string deserializeResponse = JsonConvert.SerializeObject(mainResponse);
            DataTable dt_list = (DataTable)JsonConvert.DeserializeObject("[" + deserializeResponse.Split('[')[1].Replace("}]", "").Replace(deserializeResponse.Split('[')[0].Replace("}]", ""), "") + "]", (typeof(DataTable)));
            bluck_inset_items(dt_list, det);

            //checking if next page is available

            string headers = wc.ResponseHeaders.GetValues("Paging-Headers")[0].ToString();

            DataTable dttt = (DataTable)JsonConvert.DeserializeObject("[" + headers + "]", (typeof(DataTable)));

            string nextpage = dttt.Rows[0]["nextPage"].ToString();
            if (nextpage != "Yes")
                ItemCount = 0;

            return headers;


        }
    }




    private static int ItemCount = 0;
    public static void bluck_inset_items(DataTable table, int det)
    {
        SqlConnection con1 = new SqlConnection(PAYEClass.connection);
        using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
        {
            if (ItemCount < 1)
            {
                con1.Open();
                SqlCommand comm = new SqlCommand("Delete from Assessment_Item_API where ApiId = " + det, con1);
                comm.ExecuteNonQuery();
                con1.Close();
            }
            ItemCount++;
            // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
            //foreach (DataColumn col in table.Columns)
            //{
            //    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            //}
            DataTable tbl = new DataTable();
           
            tbl.Columns.Add(new DataColumn("TaxPayerID", typeof(int)));
            tbl.Columns.Add(new DataColumn("TaxPayerTypeID", typeof(int)));
            tbl.Columns.Add(new DataColumn("TaxPayerTypeName", typeof(string)));
            tbl.Columns.Add(new DataColumn("TaxPayerRIN", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssetID", typeof(int)));
            tbl.Columns.Add(new DataColumn("AssetTypeID", typeof(int)));
            tbl.Columns.Add(new DataColumn("AssetTypeName", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssetRIN", typeof(string)));
            tbl.Columns.Add(new DataColumn("ProfileID", typeof(int))); 
            tbl.Columns.Add(new DataColumn("ProfileReferenceNo", typeof(string)));
            tbl.Columns.Add(new DataColumn("ProfileDescription", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssessmentRuleID", typeof(string))); 
            tbl.Columns.Add(new DataColumn("AssessmentRuleCode", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssessmentRuleName", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssessmentItemID", typeof(int))); 
            tbl.Columns.Add(new DataColumn("AssessmentItemReferenceNo", typeof(string))); 
            tbl.Columns.Add(new DataColumn("AssessmentGroupID", typeof(int)));
            tbl.Columns.Add(new DataColumn("AssessmentGroupName", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssessmentSubGroupID", typeof(int)));
            tbl.Columns.Add(new DataColumn("AssessmentSubGroupName", typeof(string))); 
            tbl.Columns.Add(new DataColumn("RevenueStreamID", typeof(int))); 
            tbl.Columns.Add(new DataColumn("RevenueStreamName", typeof(string))); 
            tbl.Columns.Add(new DataColumn("RevenueSubStreamID", typeof(int)));
            tbl.Columns.Add(new DataColumn("RevenueSubStreamName", typeof(string))); 
            tbl.Columns.Add(new DataColumn("AssessmentItemCategoryID", typeof(int))); 
            tbl.Columns.Add(new DataColumn("AssessmentItemCategoryName", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssessmentItemSubCategoryID", typeof(int)));
            tbl.Columns.Add(new DataColumn("AssessmentItemSubCategoryName", typeof(string)));
            tbl.Columns.Add(new DataColumn("AgencyID", typeof(int)));
            tbl.Columns.Add(new DataColumn("AgencyName", typeof(string)));
            tbl.Columns.Add(new DataColumn("AssessmentItemName", typeof(string)));
            tbl.Columns.Add(new DataColumn("ComputationID", typeof(int)));
            tbl.Columns.Add(new DataColumn("ComputationName", typeof(string))); 
            tbl.Columns.Add(new DataColumn("TaxBaseAmount", typeof(string)));
            tbl.Columns.Add(new DataColumn("Percentage", typeof(string)));
            tbl.Columns.Add(new DataColumn("TaxAmount", typeof(string))); 
            tbl.Columns.Add(new DataColumn("ApiId", typeof(int)));

      
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow dr = tbl.NewRow();
                dr["TaxPayerID"] = table.Rows[i]["TaxPayerID"];
                dr["TaxPayerTypeID"] = table.Rows[i]["TaxPayerTypeID"];
                dr["TaxPayerTypeName"] = table.Rows[i]["TaxPayerTypeName"];
                dr["TaxPayerRIN"] = table.Rows[i]["TaxPayerRIN"];
                dr["AssetID"] = table.Rows[i]["AssetID"];
                dr["AssetTypeID"] = table.Rows[i]["AssetTypeID"];
                dr["AssetTypeName"] = table.Rows[i]["AssetTypeName"];
                dr["AssetRIN"] = table.Rows[i]["AssetRIN"];
                dr["ProfileID"] = table.Rows[i]["ProfileID"];
                dr["ProfileReferenceNo"] = table.Rows[i]["ProfileReferenceNo"];
                dr["ProfileDescription"] = table.Rows[i]["ProfileDescription"];
                dr["AssessmentRuleID"] = table.Rows[i]["AssessmentRuleID"];
                dr["AssessmentRuleCode"] = table.Rows[i]["AssessmentRuleCode"];
                dr["AssessmentRuleName"] = table.Rows[i]["AssessmentRuleName"];
                dr["AssessmentItemID"] = table.Rows[i]["AssessmentItemID"];
                dr["AssessmentItemReferenceNo"] = table.Rows[i]["AssessmentItemReferenceNo"];
                dr["AssessmentGroupID"] = table.Rows[i]["AssessmentGroupID"];
                dr["AssessmentGroupName"] = table.Rows[i]["AssessmentGroupName"];
                dr["AssessmentSubGroupID"] = table.Rows[i]["AssessmentSubGroupID"];
                dr["AssessmentSubGroupName"] = table.Rows[i]["AssessmentSubGroupName"];
                dr["RevenueStreamID"] = table.Rows[i]["RevenueStreamID"];
                dr["RevenueStreamName"] = table.Rows[i]["RevenueStreamName"];
                dr["RevenueSubStreamID"] = table.Rows[i]["RevenueSubStreamID"];
                dr["RevenueSubStreamName"] = table.Rows[i]["RevenueSubStreamName"];
                dr["AssessmentItemCategoryID"] = table.Rows[i]["AssessmentItemCategoryID"];
                dr["AssessmentItemCategoryName"] = table.Rows[i]["AssessmentItemCategoryName"];
                dr["AssessmentItemSubCategoryID"] = table.Rows[i]["AssessmentItemSubCategoryID"];
                dr["AssessmentItemSubCategoryName"] = table.Rows[i]["AssessmentItemSubCategoryName"];
                dr["AgencyID"] = table.Rows[i]["AgencyID"];
                dr["AgencyName"] = table.Rows[i]["AgencyName"];
                dr["AssessmentItemName"] = table.Rows[i]["AssessmentItemName"];
                dr["ComputationID"] = table.Rows[i]["ComputationID"];
                dr["ComputationName"] = table.Rows[i]["ComputationName"];
                dr["TaxBaseAmount"] = table.Rows[i]["TaxBaseAmount"];
                dr["Percentage"] = table.Rows[i]["Percentage"];
                dr["TaxAmount"] = table.Rows[i]["TaxAmount"];
                dr["ApiId"] = det;
                tbl.Rows.Add(dr);
            }
            bulkCopy.BulkCopyTimeout = 600;
            bulkCopy.DestinationTableName = "Assessment_Item_API";
            bulkCopy.ColumnMappings.Add("TaxPayerID","TaxPayerID");
            bulkCopy.ColumnMappings.Add("TaxPayerTypeID","TaxPayerTypeID");
            bulkCopy.ColumnMappings.Add("TaxPayerTypeName","TaxPayerTypeName");
            bulkCopy.ColumnMappings.Add("TaxPayerRIN","TaxPayerRIN");
            bulkCopy.ColumnMappings.Add("AssetID","AssetID");
            bulkCopy.ColumnMappings.Add("AssetTypeID","AssetTypeID");
            bulkCopy.ColumnMappings.Add("AssetTypeName","AssetTypeName");
            bulkCopy.ColumnMappings.Add("AssetRIN","AssetRIN");
            bulkCopy.ColumnMappings.Add("ProfileID","ProfileID");
            bulkCopy.ColumnMappings.Add("ProfileReferenceNo","ProfileReferenceNo");
            bulkCopy.ColumnMappings.Add("ProfileDescription","ProfileDescription");
            bulkCopy.ColumnMappings.Add("AssessmentRuleID","AssessmentRuleID");
            bulkCopy.ColumnMappings.Add("AssessmentRuleCode","AssessmentRuleCode");
            bulkCopy.ColumnMappings.Add("AssessmentRuleName","AssessmentRuleName");
            bulkCopy.ColumnMappings.Add("AssessmentItemID","AssessmentItemID");
            bulkCopy.ColumnMappings.Add("AssessmentItemReferenceNo","AssessmentItemReferenceNo");
            bulkCopy.ColumnMappings.Add("AssessmentGroupID","AssessmentGroupID");
            bulkCopy.ColumnMappings.Add("AssessmentGroupName","AssessmentGroupName");
            bulkCopy.ColumnMappings.Add("AssessmentSubGroupID","AssessmentSubGroupID");
            bulkCopy.ColumnMappings.Add("AssessmentSubGroupName","AssessmentSubGroupName");
            bulkCopy.ColumnMappings.Add("RevenueStreamID","RevenueStreamID");
            bulkCopy.ColumnMappings.Add("RevenueStreamName","RevenueStreamName");
            bulkCopy.ColumnMappings.Add("RevenueSubStreamID","RevenueSubStreamID");
            bulkCopy.ColumnMappings.Add("RevenueSubStreamName","RevenueSubStreamName");
            bulkCopy.ColumnMappings.Add("AssessmentItemCategoryID","AssessmentItemCategoryID");
            bulkCopy.ColumnMappings.Add("AssessmentItemCategoryName","AssessmentItemCategoryName");
            bulkCopy.ColumnMappings.Add("AssessmentItemSubCategoryID","AssessmentItemSubCategoryID");
            bulkCopy.ColumnMappings.Add("AssessmentItemSubCategoryName","AssessmentItemSubCategoryName");
            bulkCopy.ColumnMappings.Add("AgencyID","AgencyID");
            bulkCopy.ColumnMappings.Add("AgencyName","AgencyName");
            bulkCopy.ColumnMappings.Add("AssessmentItemName","AssessmentItemName");
            bulkCopy.ColumnMappings.Add("ComputationID","ComputationID");
            bulkCopy.ColumnMappings.Add("ComputationName","ComputationName");
            bulkCopy.ColumnMappings.Add("TaxBaseAmount","TaxBaseAmount");
            bulkCopy.ColumnMappings.Add("Percentage","Percentage");
            bulkCopy.ColumnMappings.Add("TaxAmount","TaxAmount");
            bulkCopy.ColumnMappings.Add("ApiId", "ApiId");


            con1.Open();
            bulkCopy.WriteToServer(tbl);
            con1.Close();
            //  showmsg(1, "items synced successfully");
        }
    }
    //public static void bluck_inset_items(DataTable table, int det)
    //{

    //    SqlConnection con1 = new SqlConnection(PAYEClass.connection);
    //    using (SqlDataReader rdr = cmd3.ExecuteReader())
    //    {
    //        using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
    //        {
    //            if (ItemCount < 1)
    //            {
    //                con1.Open();
    //                SqlCommand comm = new SqlCommand("Delete from Assessment_Item_API where ApiId = " + det, con1);
    //                comm.ExecuteNonQuery();
    //                con1.Close();
    //            }
    //            ItemCount++;

    //            con1.Open();
    //            SqlConnection co1 = new SqlConnection(PAYEClass.connection);
    //            using (SqlDataReader rdr = cmd3.ExecuteReader())
    //            {
    //                using (var bulkCopy = new SqlBulkCopy(co1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
    //                {
    //                    // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
    //                    for (int i = 0; i < rdr.FieldCount; i++)
    //                    {
    //                        bulkCopy.ColumnMappings.Add(rdr.GetName(i), rdr.GetName(i));
    //                    }
    //                    bulkCopy.BulkCopyTimeout = 600;
    //                    bulkCopy.DestinationTableName = "Assessment_Item_API";
    //                    bulkCopy.WriteToServer(rdr);
    //                    //  showmsg(1, "items synced successfully");
    //                }
    //            }

    //            for (int i = 0; i < rdr.FieldCount; i++)
    //            {
    //                bulkCopy.ColumnMappings.Add(rdr.GetName(i), rdr.GetName(i));
    //            }
    //            bulkCopy.BulkCopyTimeout = 600;
    //            bulkCopy.DestinationTableName = "Assessment_Item_API";
    //            bulkCopy.WriteToServer(rdr);

    //            con1.Close();
    //        }
    //    }
    //}

    private static int AssetCount = 0;
    public static void bluck_inset_Assets(DataTable table, int det)
    {

        SqlConnection con1 = new SqlConnection(PAYEClass.connection);
        if (AssetCount < 1)
        {
            con1.Open();
            SqlCommand comm = new SqlCommand("Delete from Businesses_API where ApiId = " + det, con1);
            comm.ExecuteNonQuery();
            con1.Close();
        }
        AssetCount++;

        try
        {
            using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
            {

                con1.Open();
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    SqlCommand cmd = new SqlCommand("insert into Businesses_API values("
                        + Convert.ToInt64(table.Rows[i]["BusinessID"])
                        + ",'" + table.Rows[i]["BusinessRIN"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["AssetTypeID"])
                        + ",'" + table.Rows[i]["AssetTypeName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["BusinessTypeID"])
                        + ",'" + table.Rows[i]["BusinessTypeName"].ToString().Replace("'", "\"")
                        + "','" + table.Rows[i]["BusinessName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["LGAID"])
                        + ",'" + table.Rows[i]["LGAName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["BusinessCategoryID"])
                        + ",'" + table.Rows[i]["BusinessCategoryName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["BusinessSectorID"])
                        + ",'" + table.Rows[i]["BusinessSectorName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["BusinessSubSectorID"])
                        + ",'" + table.Rows[i]["BusinessSubSectorName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["BusinessStructureID"])
                        + ",'" + table.Rows[i]["BusinessStructureName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["BusinessOperationID"])
                        + ",'" + table.Rows[i]["BusinessOperationName"].ToString().Replace("'", "\"")
                        + "'," + Convert.ToInt32(table.Rows[i]["SizeID"])
                        + ",'" + table.Rows[i]["SizeName"].ToString().Replace("'", "\"")
                        + "','" + table.Rows[i]["ContactName"].ToString().Replace("'", "\"")
                        + "','" + table.Rows[i]["BusinessNumber"].ToString().Replace("'", "\"")
                        + "','" + table.Rows[i]["BusinessAddress"].ToString().Replace("'", "\"")
                        + "'," + det
                        + ");", con1);

                    cmd.CommandTimeout = 2 * 1000;
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {

                        continue;
                    }
                }
                con1.Close();
            }
        }
        catch (Exception e)
        {
            //dbug here if assets not being saved
            String s = e.Message;

        }

    }

    private static int ProfileCount = 0;
    public static void bluck_inset_Profile(DataTable table, int det)
    {

        SqlConnection con1 = new SqlConnection(PAYEClass.connection);
        if (ProfileCount < 1)
        {
            con1.Open();
            SqlCommand comm = new SqlCommand("Delete from Profiles_API where ApiId = " + det, con1);
            comm.ExecuteNonQuery();
            con1.Close();
        }
        ProfileCount++;
        using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
        {
            con1.Open();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("insert into Profiles_API values("
                 + Convert.ToInt64(table.Rows[i]["ProfileID"])
                 + ",'" + table.Rows[i]["ProfileReferenceNo"]
                 + "','" + table.Rows[i]["ProfileDescription"].ToString()
                 + "'," + det
                 + ")",
                 con1);
                cmd.CommandTimeout = 60 * 600;
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    continue;
                }

            }
            con1.Close();
        }

    }


    private static int IndividualCount = 0;
    public static void bluck_inset_individual(DataTable table, int det)
    {
        SqlConnection con1 = new SqlConnection(PAYEClass.connection);
        using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
        {

            if (IndividualCount < 1)
            {
                con1.Open();
                SqlCommand comm = new SqlCommand("Delete from Individuals_API where ApiId = " + det, con1);
                comm.ExecuteNonQuery();
                con1.Close();
            }
            IndividualCount++;

            con1.Open();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("insert into Individuals_API values("
                    + Convert.ToInt64(table.Rows[i]["TaxPayerID"])
                    + "," + Convert.ToInt32(table.Rows[i]["TaxPayerTypeID"])
                    + ",'" + table.Rows[i]["TaxPayerTypeName"].ToString()
                    + "','" + table.Rows[i]["TaxPayerName"].ToString().Replace("'", "\"")
                    + "','" + table.Rows[i]["TaxPayerRIN"].ToString()
                    + "','" + table.Rows[i]["MobileNumber"].ToString()
                    + "','" + table.Rows[i]["ContactAddress"].ToString()
                    + "','" + table.Rows[i]["EmailAddress"].ToString()
                    + "','" + table.Rows[i]["TIN"].ToString()
                    + "','" + table.Rows[i]["TaxOffice"].ToString()
                    + "'," + det
                    + ");",
                    con1);
                cmd.CommandTimeout = 2 * 600;
                cmd.ExecuteNonQuery();

            }
        }

    }

    private static int CorporateCount = 0;

    public static void bluck_inset_Corporate(DataTable table, int det)
    {
        SqlConnection con1 = new SqlConnection(PAYEClass.connection);
        using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
        {
            if (CorporateCount < 1)
            {
                con1.Open();
                SqlCommand comm = new SqlCommand("Delete from CompanyList_API where ApiId = " + det, con1);
                comm.ExecuteNonQuery();
                con1.Close();
            }

            CorporateCount++;

            con1.Open();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("insert into CompanyList_API values("
                    + Convert.ToInt64(table.Rows[i]["TaxPayerID"])
                    + "," + Convert.ToInt32(table.Rows[i]["TaxPayerTypeID"])
                    + ",'" + table.Rows[i]["TaxPayerTypeName"].ToString().Replace("'", "\"")
                    + "','" + table.Rows[i]["TaxPayerName"].ToString().Replace("'", "\"")
                    + "','" + table.Rows[i]["TaxPayerRIN"].ToString().Replace("'", "\"")
                    + "','" + table.Rows[i]["MobileNumber"].ToString()
                    + "','" + table.Rows[i]["ContactAddress"].ToString().Replace("'", "\"")
                    + "','" + table.Rows[i]["EmailAddress"].ToString()
                    + "','" + table.Rows[i]["TIN"].ToString()
                    + "','" + table.Rows[i]["TaxOffice"].ToString().Replace("'", "\"")
                    + "'," + det + ")",
                     con1);
                cmd.CommandTimeout = 2 * 600;
                cmd.ExecuteNonQuery();
            }
            con1.Close();
        }

    }

    public void insert_Individuals(DataTable table)
    {
        try
        {

            SqlConnection con1 = new SqlConnection(PAYEClass.connection);

            SqlCommand truncate = new SqlCommand("TRUNCATE TABLE Individuals_API", con);
            con.Open();
            truncate.ExecuteNonQuery();
            con.Close();

            SqlCommand update_tables_API_Updated = new SqlCommand("update tables_API_Updated set LastUpdatedOn=getdate() where TableName='Individuals_API'", con);
            con.Open();
            update_tables_API_Updated.ExecuteNonQuery();
            con.Close();

            Array.ForEach<DataRow>(table.Select("TaxPayerTypeID IS NULL"), row => row.Delete());
            Array.ForEach<DataRow>(table.Select("TaxPayerID IS NULL"), row => row.Delete());
            Array.ForEach<DataRow>(table.Select("TaxPayerRIN IS NULL"), row => row.Delete());

            using (var bulkCopy = new SqlBulkCopy(con1.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
            {
                //con.Open();
                // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                foreach (DataColumn col in table.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                bulkCopy.BulkCopyTimeout = 1600;
                bulkCopy.DestinationTableName = "Individuals_API";
                bulkCopy.WriteToServer(table);
            }

            //DataTable dt_fetch = new DataTable();
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    SqlDataAdapter Adp = new SqlDataAdapter("SELECT * from Individuals_API where TaxPayerId=" + table.Rows[i]["TaxPayerId"].ToString() + " and TaxPayerTypeID=" + table.Rows[i]["TaxPayerTypeID"].ToString() + " and TaxPayerRIN='" + table.Rows[i]["TaxPayerRIN"].ToString() + "' and TIN=" + table.Rows[i]["TIN"].ToString() + "", con);
            //    Adp.Fill(dt_fetch);

            //    if (dt_fetch.Rows.Count == 0)
            //    {
            //        con.Open();
            //        SqlCommand cmd = new SqlCommand("insert into Individuals_API values('" + table.Rows[i]["TaxPayerID"].ToString() + "','" + table.Rows[i]["TaxPayerTypeID"].ToString().Replace("'", "''") + "','" + table.Rows[i]["TaxPayerTypeName"].ToString() + "','" + table.Rows[i]["TaxPayerName"].ToString() + "','" + table.Rows[i]["TaxPayerRIN"].ToString() + "','" + table.Rows[i]["MobileNumber"].ToString() + "'," +
            //         "'" + table.Rows[i]["ContactAddress"].ToString().Replace("'", "''") + "','" + table.Rows[i]["EmailAddress"].ToString().Replace("'", "''") + "','" + table.Rows[i]["TIN"].ToString().Replace("'", "''") + "','" + table.Rows[i]["TaxOffice"].ToString() + "');", con);
            //        cmd.ExecuteNonQuery();
            //        con.Close();
            //    }
            //}


            showmsg(1, "Individuals Synced Successfully");
        }
        catch (Exception eI)
        {
            showmsg(2, "Error Occured. Contact to Administrator.");
        }
    }
}