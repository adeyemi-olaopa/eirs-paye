﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using System.Text;
using Newtonsoft.Json;
using System.IO;

public partial class PayeCoding : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(PAYEClass.connection);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["EditEMPFlag"] = "0";
            DataTable dt_list = new DataTable();

            txt_tax_year.Items.Add("--Select Year--");
            for (int i = DateTime.Now.Year; i >= 2014; i--)
            {
                txt_tax_year.Items.Add(i.ToString());
            }
            //  SqlDataAdapter Adp = new SqlDataAdapter("select  BusinessRIN, BusinessName, count(*) as totalcount,Tax_Year,CompanyRIN, Status  from vw_InputFile where TaxMonth='January' group by BusinessRIN ,BusinessName,Tax_Year,CompanyRIN", con);

            // SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus, (CASE WHEN CodedStatus='Coded' THEN 'Coded' ELSE 'Not Coded' END) as CodedStatus  from vw_ShowBusiness_PayeInputFile_All_Selected where FiledStatus='Filed' order by CodedStatus desc", con);

            //sourabh kaushik select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus, (CASE WHEN CodedStatus='Coded' THEN 'Coded' ELSE 'Uncoded' END) as CodedStatus  from vw_ShowBusiness_PayeInputFile_All_Selected where FiledStatus='Filed' order by CodedStatus desc

            //SqlDataAdapter Adp = new SqlDataAdapter("select distinct  b.CompanyName,b.CompanyRIN, b.CompanyTIN ,b.BusinessRIN, b.BusinessName,b.TaxYear as Tax_Year,b.totalcount,ISNULL(PayeOuputFile.Status,0) as Status, case when ISNULL(PayeOuputFile.Status,0)=0 then 'UnFiled'else 'Filed' end as FiledStatus, (CASE WHEN PayeOuputFile.Status >= 2 THEN 'Coded' ELSE 'UnCoded' END) as CodedStatus from (select  vw_Rules_Check.*,(select count(1) from PayeOuputFile where PayeOuputFile.Assessment_Year=vw_Rules_Check.TaxYear and PayeOuputFile.EmployerRIN=vw_Rules_Check.TaxPayerRIN)as totalcount  from vw_Rules_Check inner join AddPayeInputFile on AddPayeInputFile.BusinessRIN=vw_Rules_Check.BusinessRIN and AddPayeInputFile.CompanyRIN=vw_Rules_Check.CompanyRIN  and AddPayeInputFile.TaxYear=vw_Rules_Check.TaxYear  ) as b  left join  (Select distinct EmployerRIN,Status,EmployerName,Assessment_Year from PayeOuputFile ) as PayeOuputFile on b.CompanyRIN=PayeOuputFile.EmployerRIN and b.TaxYear=PayeOuputFile.Assessment_Year where PayeOuputFile.Status>=1 order by CodedStatus desc", con);
            
            // changed by avanish coz total emp not matchec
            //SqlDataAdapter Adp = new SqlDataAdapter("SELECT DISTINCT b.CompanyName,b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount,  ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus, (CASE WHEN PayeOuputFile.STATUS >= 2 THEN 'Coded' ELSE 'UnCoded' END) AS CodedStatus FROM(SELECT( SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile INNER JOIN LegacySubmissionsPAYE ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year) AS totalcount, vw_Rules_Check.* FROM vw_Rules_Check INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear) AS b LEFT JOIN(SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile) AS PayeOuputFile ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year WHERE PayeOuputFile.STATUS >= 1 ORDER BY CodedStatus DESC;", con);
            
            //            SqlDataAdapter Adp = new SqlDataAdapter("SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, (select count(*) from PayeOuputFile as p where" +
            //" p.EmployerRIN = b.CompanyRIN and p.Assessment_Year = b.TaxYear) as totalcount, ISNULL(b.status, 0) AS STATUS," +
            //   "CASE WHEN ISNULL(b.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus" +
            //   " , (CASE WHEN b.STATUS >= 2 THEN 'Coded' ELSE 'UnCoded' END) AS CodedStatus" +
            //    " from vw_PayeInputFile_N as b  WHERE b.STATUS >= 1  ORDER BY CodedStatus DESC", con);
            
            //Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            //Adp.Fill(dt_list);

            Session["dt_l"] = dt_list;
            grd_Company.DataSource = dt_list;
            grd_Company.DataBind();

            int pagesize = grd_Company.Rows.Count;
            int from_pg = 1;
            int to = grd_Company.Rows.Count;
            int totalcount = dt_list.Rows.Count;
            lblpagefrom.Text = from_pg.ToString();
            lblpageto.Text = (from_pg + pagesize - 1).ToString();
            lbltoal.Text = totalcount.ToString();

            if (totalcount < grd_Company.PageSize)
                div_paging.Style.Add("margin-top", "0px");
            else
                div_paging.Style.Add("margin-top", "-60px");
        }
    }

    private DataTable GetEmployeeCompanies(string companyRIN)
    {
        DataTable responseDt = new DataTable();
        var query = "SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, b.totalcount,  ISNULL(PayeOuputFile.STATUS, 0) " +
                    "AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus, (CASE WHEN PayeOuputFile.STATUS >= 2 THEN 'Coded' " +
                    "ELSE 'UnCoded' END) AS CodedStatus FROM(SELECT( SELECT COUNT(LegacySubmissionsPAYE.BusinessRIN) FROM PayeOuputFile INNER JOIN LegacySubmissionsPAYE " +
                    "ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear AND " +
                    "PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN " +
                    "AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year) AS totalcount, vw_Rules_Check.* FROM vw_Rules_Check INNER JOIN AddPayeInputFile " + 
                    "ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN " +
                    "AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear) AS b LEFT JOIN(SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile) " +
                    "AS PayeOuputFile ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year WHERE PayeOuputFile.STATUS >= 1 " +
                    "AND b.CompanyRIN = '" + companyRIN + "' ORDER BY CodedStatus DESC";

        SqlCommand cmd = new SqlCommand(query, con);
        con.Open();

        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
        {
            cmd.CommandTimeout = PAYEClass.defaultTimeout;
            adapter.Fill(responseDt);
        }

        con.Close();
        con.Dispose();

        return responseDt;
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        grd_Company.PageIndex = e.NewPageIndex;
        grd_Company.DataSource = Session["dt_l"];

        grd_Company.DataBind();

        if (e.NewPageIndex + 1 == 1)
        {
            lblpagefrom.Text = "1";
        }
        else
        {
            lblpagefrom.Text = ((grd_Company.Rows.Count * e.NewPageIndex) + 1).ToString();
        }

        lblpageto.Text = ((e.NewPageIndex + 1) * grd_Company.Rows.Count).ToString();

    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        DataTable dt_list_s = new DataTable();
        dt_list_s = (DataTable)Session["dt_l"];
        DataTable dt_filtered = new DataTable();
        DataView dt_v = dt_list_s.DefaultView;

        if (txt_employer_RIN.Text != "")
        {
            var companyRIN = txt_employer_RIN.Text;
            dt_filtered = GetEmployeeCompanies(companyRIN);

            //dt_v.RowFilter = "CompanyRIN like '%" + txt_employer_RIN.Text + "%' or CompanyTIN like '%" + txt_employer_RIN.Text + "%' or BusinessRIN like '%" + txt_employer_RIN.Text + "%' or tax_year like '%" + txt_employer_RIN.Text + "%' or CodedStatus like '" + txt_employer_RIN.Text + "%'";

            //if (txt_tax_year.SelectedItem.Text != "--Select Year--")
            //    dt_v.RowFilter = "(CompanyRIN like '%" + txt_employer_RIN.Text + "%' or CompanyTIN like '%" + txt_employer_RIN.Text + "%' or BusinessRIN like '%" + txt_employer_RIN.Text + "%' or tax_year like '%" + txt_employer_RIN.Text + "%' or CodedStatus like '" + txt_employer_RIN.Text + "%') and (Tax_Year like '%" + txt_tax_year.SelectedItem.Text + "%')";
        }

        //if (txt_tax_year.SelectedItem.Text != "--Select Year--" && txt_employer_RIN.Text == "")
        //    dt_v.RowFilter = "Tax_Year like '%" + txt_tax_year.SelectedItem.Text + "%'";

        //grd_Company.DataSource = dt_v;
        grd_Company.DataSource = dt_filtered;
        grd_Company.DataBind();

        int pagesize = grd_Company.Rows.Count;
        int from_pg = 1;
        int to = grd_Company.Rows.Count;
        int totalcount = dt_v.Count;
        lblpagefrom.Text = from_pg.ToString();
        lblpageto.Text = (from_pg + pagesize - 1).ToString();
        lbltoal.Text = totalcount.ToString();

        if (totalcount < grd_Company.PageSize)
            div_paging.Style.Add("margin-top", "0px");
        else
            div_paging.Style.Add("margin-top", "-60px");
    }
    protected void btn_file_selected_Click(object sender, EventArgs e)
    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);
        int check = 0;
        foreach (GridViewRow gvrow in grd_Company.Rows)
        {
           
            System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
            if (chk != null & chk.Checked)
            {
                check = 1;
            }
        }

        if (check == 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Select a Company.');</script>", false);
            return;
        }

        
        string confirmValue = Request.Form["confirm_value"];

        confirmValue = hidden1.Value;
        if (confirmValue == "Yes")
        {
            foreach (GridViewRow gvrow in grd_Company.Rows)
            {
                System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
                if (chk != null & chk.Checked)
                {
                    SqlCommand delete = new SqlCommand("Update PayeOuputFile set Status=2 where EmployerRIN='" + gvrow.Cells[0].Text + "' and Assessment_Year='" + gvrow.Cells[3].Text + "'", con);
                    con.Open();
                    delete.ExecuteNonQuery();
                    con.Close();
                }
            }
            DataTable dt_list = new DataTable();
            SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus  from vw_ShowBusiness_PayeInputFile where FiledStatus='Filed' order by CodedStatus desc", con);
            Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            Adp.Fill(dt_list);

            Session["dt_l"] = dt_list;
            grd_Company.DataSource = dt_list;
            grd_Company.DataBind();
        }

    }
   

    public void checkbox()
    {
    }
    protected void grd_Company_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //check RDM
            DataTable dt_RDM = new DataTable();
            SqlConnection conn = new SqlConnection(PAYEClass.connection);

            // SqlDataAdapter Adp_RDM = new SqlDataAdapter("select  1  from vw_ShowBusiness_PayeInputFile where ('" + e.Row.Cells[0].Text + "') in (select TaxPayerRIN from vw_PreAssessmentRDM) and ('"+ e.Row.Cells[4].Text + "') in (select AssetRIN from vw_PreAssessmentRDM) and ('" + e.Row.Cells[3].Text + "') in (select TaxYear from vw_PreAssessmentRDM)", con);

            SqlDataAdapter Adp_RDM = new SqlDataAdapter("select  1  from vw_PreAssessmentRDM where TaxPayerRIN='" + e.Row.Cells[0].Text + "' and AssetRIN='" + e.Row.Cells[4].Text + "' and TaxYear='" + e.Row.Cells[3].Text + "'", conn);
            Adp_RDM.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            Adp_RDM.Fill(dt_RDM);


            CheckBox chkbox = (CheckBox)e.Row.FindControl("chkchkbox");
            LinkButton lnk_SubmitFiling = (LinkButton)e.Row.FindControl("lnksendtoinputfile");
          
            if (e.Row.Cells[7].Text == "Coded")
            {

                chkbox.Enabled = false;
                lnk_SubmitFiling.Visible = false;
               

            }

            else
            {

                chkbox.Enabled = true;
                lnk_SubmitFiling.Visible = true;
                
            }

          

        }
    }


    protected void btn_file_reverse_Click(object sender, EventArgs e)
    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);
        int check = 0;
        GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
        string rin = grd_Company.Rows[clickedRow.RowIndex].Cells[0].Text.ToString();
        //foreach (GridViewRow gvrow in grd_Company.Rows)
        //{

        //    System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
        //    if (chk != null & chk.Checked)
        //    {
        //        check = 1;
        //    }
        //}

        //if (check == 0)
        //{
        //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Select a Company.');</script>", false);
        //    return;
        //}


        string confirmValue = Request.Form["confirm_value"];

        confirmValue = hidden1.Value;
        if (confirmValue == "Yes")
        {
            //foreach (GridViewRow gvrow in grd_Company.Rows)
            //{
            //    System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
            //    if (chk != null & chk.Checked)
            //    {
            SqlCommand update_reverse = new SqlCommand("Update PayeOuputFile set Status=0 where EmployerRIN='" + grd_Company.Rows[clickedRow.RowIndex].Cells[0].Text.ToString() + "' and Assessment_Year='" + grd_Company.Rows[clickedRow.RowIndex].Cells[3].Text.ToString() + "'", con);
                    con.Open();
                    update_reverse.ExecuteNonQuery();
                    con.Close();
            //    }
            //}
            DataTable dt_list = new DataTable();
            SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus  from vw_ShowBusiness_PayeInputFile where FiledStatus='Filed' order by CodedStatus desc", con);
            Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            Adp.Fill(dt_list);

            Session["dt_l"] = dt_list;
            grd_Company.DataSource = dt_list;
            grd_Company.DataBind();
        }

    }
}