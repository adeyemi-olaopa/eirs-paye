﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;


public partial class PayeOutputFile_N : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(PAYEClass.connection);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["EditEMPFlag"] = "0";
            DataTable dt_list = new DataTable();

            txt_tax_year.Items.Add("--Select Year--");
            for (int i = DateTime.Now.Year; i >= 2014; i--)
            {
                txt_tax_year.Items.Add(i.ToString());
            }
            //  SqlDataAdapter Adp = new SqlDataAdapter("select  BusinessRIN, BusinessName, count(*) as totalcount,Tax_Year,CompanyRIN, Status  from vw_InputFile where TaxMonth='January' group by BusinessRIN ,BusinessName,Tax_Year,CompanyRIN", con);

            // SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus,SuccessfulStatus  from vw_ShowBusiness_PayeInputFile where CodedStatus='Coded' order by SuccessfulStatus asc", con);

            //sourabh kaushik select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus,SuccessfulStatus  from vw_ShowBusiness_PayeInputFile_All_Selected where CodedStatus='Coded' order by SuccessfulStatus asc

            //SqlDataAdapter Adp = new SqlDataAdapter("select distinct  b.CompanyName,b.CompanyRIN, b.CompanyTIN ,b.BusinessRIN, b.BusinessName,b.TaxYear as Tax_Year,b.totalcount,(CASE WHEN PayeOuputFile.Status =3 THEN 'Successfull' ELSE '' END) as SuccessfulStatus from (select  vw_Rules_Check.*,(select count(1) from PayeOuputFile where PayeOuputFile.Assessment_Year=vw_Rules_Check.TaxYear and PayeOuputFile.EmployerRIN=vw_Rules_Check.TaxPayerRIN)as totalcount  from vw_Rules_Check inner join AddPayeInputFile on AddPayeInputFile.BusinessRIN=vw_Rules_Check.BusinessRIN and AddPayeInputFile.CompanyRIN=vw_Rules_Check.CompanyRIN  and AddPayeInputFile.TaxYear=vw_Rules_Check.TaxYear  ) as b  left join  (Select distinct EmployerRIN,Status,EmployerName,Assessment_Year from PayeOuputFile ) as PayeOuputFile on b.CompanyRIN=PayeOuputFile.EmployerRIN and b.TaxYear=PayeOuputFile.Assessment_Year where PayeOuputFile.Status>=2 order by SuccessfulStatus asc", con);
            //SqlDataAdapter Adp = new SqlDataAdapter("SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year,  b.totalcount,  ISNULL(PayeOuputFile.STATUS, 0) AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus,  (CASE WHEN PayeOuputFile.STATUS = 3 THEN 'Successfull' ELSE '' END) AS SuccessfulStatus FROM ( SELECT ( SELECT COUNT(1) FROM PayeOuputFile INNER JOIN LegacySubmissionsPAYE ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year ) AS totalcount,  vw_Rules_Check.* FROM vw_Rules_Check INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear ) AS b LEFT JOIN( SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year FROM PayeOuputFile ) AS PayeOuputFile ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year WHERE PayeOuputFile.STATUS >= 2 ORDER BY SuccessfulStatus ASC", con);

            //            SqlDataAdapter Adp = new SqlDataAdapter("SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year, (select count(*) from PayeOuputFile as p where" +
            //" p.EmployerRIN = b.CompanyRIN and p.Assessment_Year = b.TaxYear) as totalcount, ISNULL(b.status, 0) AS STATUS," +
            //   "CASE WHEN ISNULL(b.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus" +
            //   " , (CASE WHEN b.STATUS = 3 THEN 'Successfull' ELSE 'Pending' END) AS SuccessfulStatus" +
            //    " from vw_PayeInputFile_N as b  WHERE b.STATUS >= 2 ORDER BY SuccessfulStatus ASC", con);

            //Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;

            ////changed by avanish coz total numbers re
            //Adp.Fill(dt_list);

            Session["dt_l"] = dt_list;
            grd_Company.DataSource = dt_list;
            grd_Company.DataBind();

            int pagesize = grd_Company.Rows.Count;
            int from_pg = 1;
            int to = grd_Company.Rows.Count;
            int totalcount = dt_list.Rows.Count;
            lblpagefrom.Text = from_pg.ToString();
            lblpageto.Text = (from_pg + pagesize - 1).ToString();
            lbltoal.Text = totalcount.ToString();

            if (totalcount < grd_Company.PageSize)
                div_paging.Style.Add("margin-top", "0px");
            else
                div_paging.Style.Add("margin-top", "-60px");
        }
    }

    private DataTable GetEmployeeCompanies(string companyRIN)
    {
        DataTable responseDt = new DataTable();
        var query = "SELECT DISTINCT b.CompanyName, b.CompanyRIN, b.CompanyTIN, b.BusinessRIN, b.BusinessName, b.TaxYear AS Tax_Year,  b.totalcount,  ISNULL(PayeOuputFile.STATUS, 0) " +
                    "AS STATUS, CASE WHEN ISNULL(PayeOuputFile.STATUS, 0) = 0 THEN 'UnFiled' ELSE 'Filed' END AS FiledStatus,  (CASE WHEN PayeOuputFile.STATUS = 3 THEN 'Successfull' " + 
                    "ELSE '' END) AS SuccessfulStatus FROM ( SELECT ( SELECT COUNT(1) FROM PayeOuputFile INNER JOIN LegacySubmissionsPAYE " +
                    "ON PayeOuputFile.EmployeeRIN = LegacySubmissionsPAYE.Tp_TIN AND PayeOuputFile.Assessment_Year = vw_Rules_Check.TaxYear " +
                    "AND PayeOuputFile.EmployerRIN = vw_Rules_Check.TaxPayerRIN AND vw_Rules_Check.AssetRIN = LegacySubmissionsPAYE.BusinessRIN " +
                    "AND PayeOuputFile.Assessment_Year = LegacySubmissionsPAYE.Tax_year ) AS totalcount,  vw_Rules_Check.* FROM vw_Rules_Check " +
                    "INNER JOIN AddPayeInputFile ON AddPayeInputFile.BusinessRIN = vw_Rules_Check.BusinessRIN AND AddPayeInputFile.CompanyRIN = vw_Rules_Check.CompanyRIN " +
                    "AND AddPayeInputFile.TaxYear = vw_Rules_Check.TaxYear ) AS b LEFT JOIN( SELECT DISTINCT EmployerRIN, STATUS, EmployerName, Assessment_Year " +
                    "FROM PayeOuputFile ) AS PayeOuputFile ON b.CompanyRIN = PayeOuputFile.EmployerRIN AND b.TaxYear = PayeOuputFile.Assessment_Year " +
                    "WHERE PayeOuputFile.STATUS >= 2 AND b.CompanyRIN = '" + companyRIN + "' ORDER BY SuccessfulStatus ASC";

        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
        {
            cmd.CommandTimeout = PAYEClass.defaultTimeout;
            adapter.Fill(responseDt);
        }

        con.Close();
        con.Dispose();

        return responseDt;
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_Company.PageIndex = e.NewPageIndex;
        grd_Company.DataSource = Session["dt_l"];

        grd_Company.DataBind();

        if (e.NewPageIndex + 1 == 1)
        {
            lblpagefrom.Text = "1";
        }
        else
        {
            lblpagefrom.Text = ((grd_Company.Rows.Count * e.NewPageIndex) + 1).ToString();
        }

        lblpageto.Text = ((e.NewPageIndex + 1) * grd_Company.Rows.Count).ToString();

    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        DataTable dt_list_s = new DataTable();
        dt_list_s = (DataTable)Session["dt_l"];
        DataTable dt_filtered = new DataTable();
        DataView dt_v = dt_list_s.DefaultView;

        if (txt_employer_RIN.Text != "")
        {
            var companyRIN = txt_employer_RIN.Text;
            dt_filtered = GetEmployeeCompanies(companyRIN);

            //dt_v.RowFilter = "CompanyRIN like '%" + txt_employer_RIN.Text + "%' or CompanyName like '%" + txt_employer_RIN.Text + "%' or CompanyTIN like '%" + txt_employer_RIN.Text + "%' or Tax_Year like '%" + txt_employer_RIN.Text + "%' or BusinessRIN like '%" + txt_employer_RIN.Text + "%' or SuccessfulStatus like '" + txt_employer_RIN.Text + "%'";

            //if (txt_tax_year.SelectedItem.Text != "--Select Year--")
            //    dt_v.RowFilter = "(CompanyRIN like '%" + txt_employer_RIN.Text + "%' or CompanyName like '%" + txt_employer_RIN.Text + "%' or CompanyTIN like '%" + txt_employer_RIN.Text + "%' or Tax_Year like '%" + txt_employer_RIN.Text + "%' or BusinessRIN like '%" + txt_employer_RIN.Text + "%' or SuccessfulStatus like '" + txt_employer_RIN.Text + "%') and (Tax_Year like '%" + txt_tax_year.SelectedItem.Text + "%')";
        }

        //if (txt_tax_year.SelectedItem.Text != "--Select Year--" && txt_employer_RIN.Text == "")
        //    dt_v.RowFilter = "Tax_Year like '%" + txt_tax_year.SelectedItem.Text + "%'";

        //if (txt_employer_name.Text != "")
        //    dt_v.RowFilter = "CompanyName='" + txt_employer_RIN.Text + "'";

        //if (txt_employer_TIN.Text != "")
        //    dt_v.RowFilter = "CompanyTIN='" + txt_employer_RIN.Text + "'";

        //if (txt_tax_year.Text != "")
        //    dt_v.RowFilter = "Tax_Year='" + txt_employer_RIN.Text + "'";

        //if (txt_business_RIN.Text != "")
        //    dt_v.RowFilter = "BusinessRIN='" + txt_employer_RIN.Text + "'";


        //grd_Company.DataSource = dt_v;
        grd_Company.DataSource = dt_filtered;
        grd_Company.DataBind();

        int pagesize = grd_Company.Rows.Count;
        int from_pg = 1;
        int to = grd_Company.Rows.Count;
        int totalcount = dt_v.Count;
        lblpagefrom.Text = from_pg.ToString();
        lblpageto.Text = (from_pg + pagesize - 1).ToString();
        lbltoal.Text = totalcount.ToString();

        if (totalcount < grd_Company.PageSize)
            div_paging.Style.Add("margin-top", "0px");
        else
            div_paging.Style.Add("margin-top", "-60px");
    }


    protected void btn_file_selected_Click(object sender, EventArgs e)
    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);
        int check = 0;
        foreach (GridViewRow gvrow in grd_Company.Rows)
        {
            
            System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
            if (chk != null & chk.Checked)
            {
                check = 1;
            }
            
        }
        
        if(check==0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Select a Company');</script>", false);
            return;
        }
        
        string confirmValue = Request.Form["confirm_value"];

        confirmValue = hidden1.Value;
        if (confirmValue == "Yes")
        {
            foreach (GridViewRow gvrow in grd_Company.Rows)
            {
                System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
                if (chk != null & chk.Checked)
                {
                    SqlCommand Update = new SqlCommand("Update PayeOuputFile set Status=3 where EmployerRIN='" + gvrow.Cells[0].Text + "' and Assessment_Year='" + gvrow.Cells[3].Text + "'", con);
                    con.Open();
                    Update.ExecuteNonQuery();
                    con.Close();
                }
            }
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Status Changed Successfully!!!');</script>", false);
            return;
            
            //comment by Avanish there is no need to refresh it
            //  DataTable dt_list = new DataTable();
            ////  SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus,SuccessfulStatus  from vw_ShowBusiness_PayeInputFile where CodedStatus='Coded' order by SuccessfulStatus asc", con);

            //  SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus,SuccessfulStatus  from vw_ShowBusiness_PayeInputFile_All_Selected where CodedStatus='Coded' order by SuccessfulStatus asc", con);
            //  Adp.Fill(dt_list);

            //  Session["dt_l"] = dt_list;
            //  grd_Company.DataSource = dt_list;
            //  grd_Company.DataBind();
        }

    }


    public void checkbox()
    {
    }
    protected void grd_Company_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataTable dt_list = new DataTable();
            SqlConnection conn = new SqlConnection(PAYEClass.connection);

            // SqlDataAdapter Adp = new SqlDataAdapter("select * from PreAssessmentRDM a, companies_API b where CompanyID=TaxPayerID and CompanyRIN='" + e.Row.Cells[0].Text + "'", con);

            SqlDataAdapter Adp = new SqlDataAdapter("select * from PreAssessmentRDM a, CompanyList_API b where a.TaxPayerID=b.TaxPayerID and TaxPayerRIN='" + e.Row.Cells[0].Text + "'", conn);
            Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            Adp.Fill(dt_list);
            LinkButton lnk_TaxAnalysis = (LinkButton)e.Row.FindControl("lnkTaxAnalysis");
            if (dt_list.Rows.Count > 0)
            {
                lnk_TaxAnalysis.Visible = true;
            }
            else
            {
                lnk_TaxAnalysis.Visible = false;
            }

            LinkButton lnk_reverse_Input = (LinkButton)e.Row.FindControl("lnk_reverse_Input");
            LinkButton btn_file_selected = (LinkButton)e.Row.FindControl("btn_file_selected");
            CheckBox chkbox = (CheckBox)e.Row.FindControl("chkchkbox");
           // LinkButton lnk_SubmitFiling = (LinkButton)e.Row.FindControl("lnksendtoinputfile");

            //check RDM
            DataTable dt_RDM = new DataTable();

            // SqlDataAdapter Adp_RDM = new SqlDataAdapter("select  1  from vw_ShowBusiness_PayeInputFile where ('" + e.Row.Cells[0].Text + "') in (select TaxPayerRIN from vw_PreAssessmentRDM) and ('"+ e.Row.Cells[4].Text + "') in (select AssetRIN from vw_PreAssessmentRDM) and ('" + e.Row.Cells[3].Text + "') in (select TaxYear from vw_PreAssessmentRDM)", con);

            SqlDataAdapter Adp_RDM = new SqlDataAdapter("select  1  from vw_PreAssessmentRDM where TaxPayerRIN='" + e.Row.Cells[0].Text + "' and AssetRIN='" + e.Row.Cells[4].Text + "' and TaxYear='" + e.Row.Cells[3].Text + "'", conn);
            Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            Adp_RDM.Fill(dt_RDM);


            if (e.Row.Cells[7].Text == "Successful")
            {

                chkbox.Enabled = false;
             //   lnk_SubmitFiling.Enabled = false;

              
              //  btn_file_selected.Visible = false;

            }

            else
            {

                chkbox.Enabled = true;
               // lnk_SubmitFiling.Enabled = true;
               // btn_file_selected.Visible = true;
               
            }

          

        }
    }


    //protected void btn_file_reverse_Click(object sender, EventArgs e)
    //{
    //    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);
    //    int check = 0;
    //    foreach (GridViewRow gvrow in grd_Company.Rows)
    //    {

    //        System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
    //        if (chk != null & chk.Checked)
    //        {
    //            check = 1;
    //        }
    //    }

    //    if (check == 0)
    //    {
    //        ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Select a Company.');</script>", false);
    //        return;
    //    }


    //    string confirmValue = Request.Form["confirm_value"];

    //    confirmValue = hidden1.Value;
    //    if (confirmValue == "Yes")
    //    {
    //        foreach (GridViewRow gvrow in grd_Company.Rows)
    //        {
    //            System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
    //            if (chk != null & chk.Checked)
    //            {
    //                SqlCommand delete = new SqlCommand("Update PayeOuputFile set Status=0 where EmployerRIN='" + gvrow.Cells[0].Text + "' and Assessment_Year='" + gvrow.Cells[3].Text + "'", con);
    //                con.Open();
    //                delete.ExecuteNonQuery();
    //                con.Close();
    //            }
    //        }
    //        DataTable dt_list = new DataTable();
    //        SqlDataAdapter Adp = new SqlDataAdapter("select  CompanyName, CompanyRIN, CompanyTIN, BusinessRIN, BusinessName, totalcount,Tax_Year,CompanyRIN, Status,FiledStatus,CodedStatus  from vw_ShowBusiness_PayeInputFile where FiledStatus='Filed' order by CodedStatus desc", con);
    //        Adp.Fill(dt_list);

    //        Session["dt_l"] = dt_list;
    //        grd_Company.DataSource = dt_list;
    //        grd_Company.DataBind();
    //    }

    //}


    protected void btn_file_reverse_Click(object sender, EventArgs e)
    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);
        int check = 0;
        GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
        string rin = grd_Company.Rows[clickedRow.RowIndex].Cells[0].Text.ToString();
        //foreach (GridViewRow gvrow in grd_Company.Rows)
        //{

        //    System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
        //    if (chk != null & chk.Checked)
        //    {
        //        check = 1;
        //    }
        //}

        //if (check == 0)
        //{
        //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Please Select a Company.');</script>", false);
        //    return;
        //}


        string confirmValue = Request.Form["confirm_value"];

        confirmValue = hidden1.Value;
        if (confirmValue == "Yes")
        {
            //foreach (GridViewRow gvrow in grd_Company.Rows)
            //{
            //    System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvrow.FindControl("chkchkbox");
            //    if (chk != null & chk.Checked)
            //    {
            SqlCommand update_reverse = new SqlCommand("Update PayeOuputFile set Status=0 where EmployerRIN='" + grd_Company.Rows[clickedRow.RowIndex].Cells[0].Text.ToString() + "' and Assessment_Year='" + grd_Company.Rows[clickedRow.RowIndex].Cells[3].Text.ToString() + "'", con);
            con.Open();
            update_reverse.ExecuteNonQuery();
            con.Close();
            //    }
            //}
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

    }


    protected void btn_Save_Input_Ouput_Click(object sender, EventArgs e)
    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "confirm('Unable to locate your search item. Do you want to search the closest match from your item?');", true);
        int check = 0;
        GridViewRow clickedRow = ((LinkButton)sender).NamingContainer as GridViewRow;
        string rin = grd_Company.Rows[clickedRow.RowIndex].Cells[0].Text.ToString();
        string tax_year = grd_Company.Rows[clickedRow.RowIndex].Cells[3].Text.ToString();


        DataTable dt_list_SaveInputOutput = new DataTable();

        SqlDataAdapter Adp = new SqlDataAdapter("select * from vw_SaveInputOutput_API where EmployerRIN='" + rin + "' and Assessment_Year='" + tax_year + "'", con);
        Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
        Adp.Fill(dt_list_SaveInputOutput);


        for (int i = 0; i < dt_list_SaveInputOutput.Rows.Count; i++)
        {
            /************************ADD Input API***********************/

            //DateTime.ParseExact(row1["start_month"].ToString(), "MMM", CultureInfo.InvariantCulture).Month
            string[] res_Input_API;
            string URI_Input_API = "https://stage-api.eirsautomation.xyz/DataWarehouse/PAYEInput/Insert";
            URI_Input_API = PAYEClass.URL_API + "DataWarehouse/PAYEInput/Insert";

            // string myParameters_Input_API = "{\n    \"TranscationDate\": \"" + DateTime.Now.Date.ToString() + "\",\n    \"Employer_RIN\": \"" + drpfupcomapny.SelectedValue.ToString() + "\",\n    \"Employee_RIN\": \"" + dt_list_api.Rows[0]["TaxPayerRIN"].ToString().Trim() + "\",\n    \"Assessment_Year\": " + row1["assessment_year"].ToString().Trim() + ",\n    \"Start_Month\": " + DateTime.ParseExact(row1["start_month"].ToString().Substring(0, 3), "MMM", CultureInfo.InvariantCulture).Month + ",\n    \"End_Month\": " + DateTime.ParseExact(row1["end_month"].ToString().Substring(0, 3), "MMM", CultureInfo.InvariantCulture).Month + ",\n    \"Annual_Basic\": " + row1["annual_basic"].ToString() + ",\n    \"Annual_Rent\": " + row1["annual_rent"].ToString() + ",\n    \"Annual_Transport\": " + row1["annual_transport"].ToString() + ",\n    \"Annual_Utility\": " + row1["annual_utility"].ToString() + ",\n    \"Annual_Meal\": " + row1["annual_meal"].ToString() + ",\n    \"Other_Allowances_Annual\": " + row1["other_allowances_annual"].ToString().Trim() + ",\n    \"Leave_Transport_Grant_Annual\": " + row1["leave_transport_grant_annual"].ToString().Trim() + ",\n    \"pension_contribution_declared\": " + row1["pension_contribution_declared"].ToString().Trim() + ",\n    \"nhf_contribution_declared\": " + row1["nhf_contribution_declared"].ToString().Trim() + ",\n    \"nhis_contribution_declared\": " + row1["nhis_contribution_declared"].ToString().Trim() + "\n}";

            string tax_of = "For Review";
          //  tax_of = Session["TaxOfc"].ToString();
            string myParameters_Input_API = "{\n    \"TranscationDate\": \"" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "\",\n    \"Employer_RIN\": \"" + dt_list_SaveInputOutput.Rows[i]["EmployerRIN"].ToString() + "\",\n    \"Employee_RIN\": \"" + dt_list_SaveInputOutput.Rows[i]["EmployeeRIN"].ToString() + "\",\n    \"Assessment_Year\": " + dt_list_SaveInputOutput.Rows[i]["Assessment_Year"].ToString() + ",\n    \"Start_Month\": " + DateTime.ParseExact(dt_list_SaveInputOutput.Rows[i]["StartMonth"].ToString().Substring(0, 3), "MMM", CultureInfo.InvariantCulture).Month + ",\n    \"End_Month\": " + DateTime.ParseExact(dt_list_SaveInputOutput.Rows[i]["EndMonth"].ToString().Substring(0, 3), "MMM", CultureInfo.InvariantCulture).Month + ",\n    \"Annual_Basic\": " + dt_list_SaveInputOutput.Rows[i]["AnnualBasic"].ToString() + ",\n    \"Annual_Rent\": " + dt_list_SaveInputOutput.Rows[i]["AnnualRent"].ToString() + ",\n    \"Annual_Transport\": " + dt_list_SaveInputOutput.Rows[i]["AnnualTransport"].ToString() + ",\n    \"Annual_Utility\": " + dt_list_SaveInputOutput.Rows[i]["AnnualUtility"].ToString() + ",\n    \"Annual_Meal\": " + dt_list_SaveInputOutput.Rows[i]["AnnualMeal"].ToString() + ",\n    \"Other_Allowances_Annual\": " + dt_list_SaveInputOutput.Rows[i]["OtherAllowances_Annual"].ToString() + ",\n    \"Leave_Transport_Grant_Annual\": " + dt_list_SaveInputOutput.Rows[i]["LeaveTransport_Annual"].ToString().Trim() + ",\n    \"pension_contribution_declared\": " + dt_list_SaveInputOutput.Rows[i]["Pension"].ToString().Trim() + ",\n    \"nhf_contribution_declared\": " + dt_list_SaveInputOutput.Rows[i]["NHF"].ToString().Trim() + ",\n    \"nhis_contribution_declared\": " + dt_list_SaveInputOutput.Rows[i]["NHIS"].ToString().Trim() + ",\n    \"Tax_Office\":\"" + dt_list_SaveInputOutput.Rows[i]["TaxOffice"].ToString().Trim() + "\"\n}";

            string InsCompRes_InputAPI = "";
            using (WebClient wc_Input_API = new WebClient())
            {
                wc_Input_API.Headers[HttpRequestHeader.ContentType] = "application/json";
                wc_Input_API.Headers[HttpRequestHeader.Authorization] = "Bearer " + Session["token"].ToString();
                // string json = JsonConvert.SerializeObject(Assessment);
                InsCompRes_InputAPI = wc_Input_API.UploadString(URI_Input_API, myParameters_Input_API);

                res_Input_API = InsCompRes_InputAPI.Split('"');

            }
            /****************************END***************************/

            /************************ADD Ouput API***********************/
            //  string tax_of = "For Review";
            //DateTime.ParseExact(row1["start_month"].ToString(), "MMM", CultureInfo.InvariantCulture).Month
            string[] res_Ouput_API;
            string URI_Ouput_API = "https://stage-api.eirsautomation.xyz/DataWarehouse/PAYEOutput/Insert";
            URI_Ouput_API = PAYEClass.URL_API + "DataWarehouse/PAYEOutput/Insert";
            // string myParameters_Ouput_API = "{\n    \"Transaction_Date\": \"" + DateTime.Now.ToString() + "\",\n    \"Employee_Rin\": \"" + dt.Rows[i]["employee_rin"].ToString().Replace("'", "''") + "\",\n    \"Employer_Rin\": \"" + drpfupcomapny.SelectedValue.ToString() + "\",\n    \"AssessmentYear\": " + Convert.ToInt32(Session["Tax_Year"].ToString()) + ",\n    \"Assessment_Month\": 1,\n    \"Monthly_CRA\": " + sal_brkup[0] + ",\n    \"Monthly_Gross\": 0,\n    \"Monthly_ValidatedNHF\": " + sal_brkup[2] + ",\n    \"Monthly_ValidatedNHIS\": " + sal_brkup[3] + ",\n    \"Monthly_ValidatedPension\": " + sal_brkup[1] + ",\n    \"Monthly_TaxFreePay\": " + sal_brkup[4] + ",\n    \"Monthly_ChargeableIncome\": " + sal_brkup[5] + ",\n    \"Monthly_Tax\": " + sal_brkup[7] + "\n}";
            string myParameters_Ouput_API = "{\n    \"Transaction_Date\": \"" + DateTime.Now.ToString("yyyy-MM-dd") + "\",\n    \"Employee_Rin\": \"" + dt_list_SaveInputOutput.Rows[i]["employeerin"].ToString().Replace("'", "''") + "\",\n    \"Employer_Rin\": \"" + dt_list_SaveInputOutput.Rows[i]["EmployerRIN"].ToString() + "\",\n    \"AssessmentYear\": " + Convert.ToInt32(dt_list_SaveInputOutput.Rows[i]["Assessment_Year"].ToString()) + ",\n    \"Assessment_Month\": 1,\n    \"Monthly_CRA\": " + dt_list_SaveInputOutput.Rows[i]["CRA"].ToString().Trim() + ",\n    \"Monthly_Gross\": 0,\n    \"Monthly_ValidatedNHF\": " + dt_list_SaveInputOutput.Rows[i]["ValidatedNHF"].ToString().Trim() + ",\n    \"Monthly_ValidatedNHIS\": " + dt_list_SaveInputOutput.Rows[i]["ValidatedNHIS"].ToString().Trim() + ",\n    \"Monthly_ValidatedPension\": " + dt_list_SaveInputOutput.Rows[i]["ValidatedPension"].ToString().Trim() + ",\n    \"Monthly_TaxFreePay\": " + dt_list_SaveInputOutput.Rows[i]["TaxFreePay"].ToString().Trim() + ",\n    \"Monthly_ChargeableIncome\": " + dt_list_SaveInputOutput.Rows[i]["ChargeableIncome"].ToString().Trim() + ",\n    \"Monthly_Tax\": " + dt_list_SaveInputOutput.Rows[i]["MonthlyTax"].ToString().Trim() + ",\n    \"Tax_Office\":\"" + dt_list_SaveInputOutput.Rows[i]["TaxOffice"].ToString().Trim() + "\"\n}";
            string InsCompRes_OuputAPI = "";
            using (WebClient wc_Ouput_API = new WebClient())
            {
                wc_Ouput_API.Headers[HttpRequestHeader.ContentType] = "application/json";
                wc_Ouput_API.Headers[HttpRequestHeader.Authorization] = "Bearer " + Session["token"].ToString();
                // string json = JsonConvert.SerializeObject(Assessment);
                InsCompRes_OuputAPI = wc_Ouput_API.UploadString(URI_Ouput_API, myParameters_Ouput_API);

                res_Ouput_API = InsCompRes_OuputAPI.Split('"');

            }
            /****************************END***************************/
        }

        ScriptManager.RegisterStartupScript(Page, this.GetType(), "AlertMessage", "<script language=\"javascript\"  type=\"text/javascript\">;alert('Process Completed.');</script>", false);
        return;
    }
}