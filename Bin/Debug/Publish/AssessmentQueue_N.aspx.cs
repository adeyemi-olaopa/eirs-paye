﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using System.Text;
using Newtonsoft.Json;
using System.IO;

public partial class AssessmentQueue_N : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(PAYEClass.connection);
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            txt_tax_year.Items.Add("--Select Year--");
            for (int i = DateTime.Now.Year; i >= 2014; i--)
            {
                txt_tax_year.Items.Add(i.ToString());
            }

            bindgrid();   
        }
    }
    protected void grdAssessmentQueue_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdAssessmentQueue.PageIndex = e.NewPageIndex;
        grdAssessmentQueue.DataSource = Session["dtempcollection"];

        grdAssessmentQueue.DataBind();
    }

    public void bindgrid()
    {
        try
        {
            DataTable dtempcollection = new DataTable();
            dtempcollection.Columns.Add("EmployerRIN", typeof(string));
            dtempcollection.Columns.Add("EmployerName", typeof(string));
            dtempcollection.Columns.Add("Asset", typeof(string));
            dtempcollection.Columns.Add("Rule", typeof(string));
            dtempcollection.Columns.Add("TaxMonthYear", typeof(string));
            dtempcollection.Columns.Add("TaxBaseAmount", typeof(string));
            dtempcollection.Columns.Add("AssessmentNotes", typeof(string));
            dtempcollection.Columns.Add("Status", typeof(string));
            dtempcollection.Columns.Add("AssessmentRef", typeof(string));

            DataTable dt_list = new DataTable();
            SqlDataAdapter Adp = new SqlDataAdapter("select * from vw_PreAssessmentRDM", con);
            Adp.SelectCommand.CommandTimeout = PAYEClass.defaultTimeout;
            Adp.Fill(dt_list);

            for (int i = 0; i < dt_list.Rows.Count; i++)
            {
               // dtempcollection.Rows.Add(dt_list.Rows[i]["TaxPayerRIN"].ToString(), dt_list.Rows[i]["TaxPayerName"].ToString(), dt_list.Rows[i]["AssetRIN"].ToString(), dt_list.Rows[i]["AssessmentRuleName"].ToString(), dt_list.Rows[i]["TaxYear"].ToString(), dt_list.Rows[i]["TaxBaseAmount"].ToString(), dt_list.Rows[i]["AssessmentRuleName"].ToString().Split('-')[1].Replace("Collections", "") + "(" + dt_list.Rows[i]["TaxBaseAmount"].ToString() + ")", dt_list.Rows[i]["Status"].ToString(), dt_list.Rows[i]["Assessment_RefNo"].ToString());

                dtempcollection.Rows.Add(dt_list.Rows[i]["TaxPayerRIN"].ToString(), dt_list.Rows[i]["TaxPayerName"].ToString(), dt_list.Rows[i]["AssetRIN"].ToString(), dt_list.Rows[i]["AssessmentRuleName"].ToString(), dt_list.Rows[i]["TaxYear"].ToString(), dt_list.Rows[i]["TaxBaseAmount"].ToString(), dt_list.Rows[i]["AssessmentRuleName"].ToString() + "-" + dt_list.Rows[i]["TaxYear"].ToString() + "(N" + dt_list.Rows[i]["TaxBaseAmount"].ToString() + ")", dt_list.Rows[i]["Status"].ToString(), dt_list.Rows[i]["Assessment_RefNo"].ToString());
            }

            Session["dtempcollection"] = dtempcollection;
            grdAssessmentQueue.DataSource = dtempcollection;
            grdAssessmentQueue.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        DataTable dt_list_s = new DataTable();
        dt_list_s = (DataTable)Session["dtempcollection"];
        // DataRow[] filteredRows = dt_list_s.Select("TaxPayerRIN LIKE '" + txt_RIN.Text + "'");
        DataTable dt_filtered = new DataTable();
        DataView dt_v = dt_list_s.DefaultView;
        if (txt_employer_RIN.Text != "")
        {
            dt_v.RowFilter = "EmployerRIN like '%" + txt_employer_RIN.Text + "%' or Asset like '%" + txt_employer_RIN.Text + "%' or Rule like '%" + txt_employer_RIN.Text + "%' or AssessmentRef like '%" + txt_employer_RIN.Text + "%' or Status like '" + txt_employer_RIN.Text + "%'";

            if (txt_tax_year.SelectedItem.Text != "--Select Year--")
                dt_v.RowFilter = "(EmployerRIN like '%" + txt_employer_RIN.Text + "%' or Asset like '%" + txt_employer_RIN.Text + "%' or Rule like '%" + txt_employer_RIN.Text + "%' or AssessmentRef like '%" + txt_employer_RIN.Text + "%' or Status like '" + txt_employer_RIN.Text + "%') and (TaxMonthYear like '%" + txt_tax_year.SelectedItem.Text + "%')";


        }
        if (txt_tax_year.SelectedItem.Text != "--Select Year--" && txt_employer_RIN.Text == "")
            dt_v.RowFilter = "TaxMonthYear like '%" + txt_tax_year.SelectedItem.Text + "%'";



        grdAssessmentQueue.DataSource = dt_v;
        grdAssessmentQueue.DataBind();

        int pagesize = grdAssessmentQueue.Rows.Count;
        int from_pg = 1;
        int to = grdAssessmentQueue.Rows.Count;
        int totalcount = dt_v.Count;
        //lblpagefrom.Text = from_pg.ToString();
        //lblpageto.Text = (from_pg + pagesize - 1).ToString();
        //lbltoal.Text = totalcount.ToString();

        //if (totalcount < grd_Company.PageSize)
        //    div_paging.Style.Add("margin-top", "0px");
        //else
        //    div_paging.Style.Add("margin-top", "-60px");
    }
}